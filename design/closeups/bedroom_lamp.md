# Bedroom lamp

#closeup #bedroom 

We look into the lamp shade, and discover that [bedroom_hook](../items/bedroom_hook.md) holds the lamp. 

The only item visible is [bedroom_hook](../items/bedroom_hook.md), exit goes back to [bedroom](../locations/bedroom.md).