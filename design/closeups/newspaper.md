# Newspaper

#closeup 

A newspaper called "The Standard". There's a big article with a photo of [igino](characters/igino.md), and several articles around it.

The articles read:

**MOB BOSS ON TRIAL** by Tyrice Curtis

[igino](characters/igino.md) Varone (shown left), the alleged head of "La Famiglia", the local crime organization, will be on trial this friday afternoon after having been accused of killing a shop owner in the lower east side.

District Attorney [brianna](characters/brianna.md) Allison tells The Standard that she will aim for a life sentence. "We have solid evidence", Misses Allison says, "We also have a witness, so I have no doubt that the jury will rule in our favor. It's time that we put an end to the business of La Famiglia!"

The trial will start on Friday, 4pm, court room 10.

**HOMELESS SHELTER GOES UP IN FLAMES** by Jasmine Guyot

East Village - a building used as a homeless shelter burnt down to the ground yesterday evening. Officer Mike Greyson, who was the first at the crime scene, told The Standard that arson can not be ruled out at this point. Until the police has other leads, the remains of the shelter will be treated as a crime scene. It's recommended to stay clear of the area due to the probable instability of the building. The gruesome numbers are as of yet 12 casualties and 34 people with burn injuries, that have been taken to Beth Israel Hospital.

**VISIT SWEDEN! A TRAVEL COMMENTARY** by Carol Reed

The Standard's own Carol Reed is travelling Europe and reports all about the nicest destinations in the old world.

This time, Carol visits the town of Norrköping, just about 120 miles east of Stockholm, Sweden's capital. Read all about Sweden's wonderful countryside and art on page 14.

**POLICE NOTICES**

Police is investigating after a dumpster was found Monday night filled with dozens of pounds of cocaine. Police says the 60 pounds of cocaine was estimated to be worth $6.6 million.
A person, suspected of a crime related to the incident, is held in police custody awaiting a court hearing.

**BEST BBQ-RECIPES BY STAR-CHEF OLIVER KARDUM**

In this edition only. See page 15!

Additionally, the following ads are visible:

- **GRAND OPENING! PHILIPP URLICH EXHIBITION AT THE MARK FERRARI GALLERY**

- **CHOLMONDELEY MOTORS - LOW PRICES, GREAT CARS**

- **JORDAN BEN - YOUR EXPERT FOR TAROT AND PSYCHIC CONSULTATION**

- **BIGGE'S BIG BOUNCY BOWL - FUN FOR THE WHOLE FAMILY. ASK ABOUT OUR BIRTHDAY SPECIALS!**