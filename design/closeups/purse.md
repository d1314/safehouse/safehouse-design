# Purse

#closeup 

A closeup of an opened, pink purse with movable items stacked one of another. The items can be clicked and vanish. Play [taking_item](../sfx/taking_item.md) upon taking each item.

The exit leads back to [bedroom](../locations/bedroom.md)

The stash is:

- [picture](../items/picture.md)
- [money](../items/money.md)
- [credit_card](../items/credit_card.md)
- [hairpin](../items/hairpin.md)
- [pin_note](../items/pin_note.md)

