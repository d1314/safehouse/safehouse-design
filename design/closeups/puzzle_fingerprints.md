# Fingerprint puzzle

#closeup 

We see the two tapes with fingerprints on the living room table.

One of the tapes is movable and has to be adjusted (rotated, panned) so that the two fingerprints match. Clicking/Touching rotates the tape. The puzzle is solved when the tape is correctly (to some degree) rotated and panned over the other tape.

See [PUZZLE 3 6 Comparing fingerprints](../gdd.md#PUZZLE%203%206%20Comparing%20fingerprints) after solving the puzzle.

There's no exit in this closeup.