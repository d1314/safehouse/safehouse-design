# Suitcase

#closeup 

An open, small, pink [suitcase](../items/suitcase.md) for Marjorie's stay in the house. We can look inside it, and it's neatly and cleanly packed.

There is a stack of items upon each other, which can be clicked and trigger responses. Each click plays [wardrobe_moving](../sfx/wardrobe_moving.md) and removes the item.

The exit leads to [bedroom](../locations/bedroom.md).

# Unpacking

Those items are presented in order:

- [shirts](../items/shirts.md)
- [sweaters](../items/sweaters.md)
- Today's [newspaper](../items/newspaper.md). Swiches to [newspaper](closeups/newspaper.md) (Play [unfolding_paper](sfx/unfolding_paper.md))
- [pants](../items/pants.md)

See [PUZZLE 1 1 Unpack](../gdd.md#PUZZLE%201%201%20Unpack)

# Unpacking, cont'd

After the phone crashed, these items remain:

- [toiletries_bag](../items/toiletries_bag.md)
- [panties](../items/panties.md)
- [bras](../items/bras.md)
- [purse](items/purse.md)

We briefly see, that the suitcase is empty now.

See [PUZZLE 1 1 Unpack](../gdd.md#PUZZLE%201%201%20Unpack)