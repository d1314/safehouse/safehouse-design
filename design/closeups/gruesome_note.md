# Gruesome note

#closeup 

The gruesome note is a worn-out paper with what appears to be an eerie child poem that goes like this:

The itsy, bitsy spider
went up the pyramid
down came the sphere
and rolled right over it.

Then fell the cube and
crushed it right and then,
the really ugly spider
could never climb again.

