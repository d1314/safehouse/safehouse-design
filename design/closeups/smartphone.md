# Smartphone

#closeup

A rather cheap [smartphone](../items/smartphone.md). It displays several screens:

# Screens

## PIN screen

Used at [PUZZLE 1 4 Finding the PIN](../gdd.md#PUZZLE%201%204%20Finding%20the%20PIN)

Shows "Please enter the PIN of your SIM card" with standard number buttons with characters.

Pressing the number buttons plays [dialtones](../sfx/touchtones.md).

Using 4872 (the number shown on the note in the [purse](purse.md)) as the pin yields:

> [marjorie](characters/marjorie.md)
>
> No, that's my credit card pin. I can remember that, because I use it more often.

When the right PIN (buttons of characters for RUBE(=7823)) is entered, we shortly see the dashboard.

There's a big analog clock showing 11:55. Under it we see the following icons in two rows of three:

- Mail
- Browser
- Photos
- Music
- Game icon from Mikael's new game
- Messenger

On the top border we see a wifi symbol and a cellular sign of the provider "Merscom".

## Finding wifi

Used at [PUZZLE 2 3 Finding the wifi router](../gdd.md#PUZZLE%202%203%20Finding%20the%20wifi%20router)

> [marjorie](../characters/marjorie.md)
> Well, the SIM card's gone. But maybe there's a wifi around.

We shortly see the dashboard, then the Wi-Fi symbol is highlighted (as if touched) and the screen changes to a list of Wi-Fi's with a strength indicator.

The WIFI Bluth1155 is the only one shown.

The Wi-Fi signal display is set to 2 of 4 bars in the [kitchen](locations/kitchen.md) and the [living_room](locations/living_room.md). It shows 3 of 4 bars in the [hall](locations/hall.md) and the [bedroom](locations/bedroom.md). It shows 4 of 4 bars in the [upper_hall](locations/upper_hall.md).

The Wi-Fi can be clicked, upon which a prompt asks for "Please enter Wi-Fi password for Bluth1155".

> [marjorie](../characters/marjorie.md)
> (grunts) Locked with a password. Just my luck. No where can I find that password?

A virtual keyboard (only characters) is displayed. Pressing its keys plays [key_sounds](../sfx/key_sounds.md)

If the wifi signal is read in [upper_hall](../locations/upper_hall.md):

> [marjorie](../characters/marjorie.md)
> The signal is high here. Maybe... the router's around here...
> (searches) Huh, there's a board loose here. I see lights flickering through the cracks.
> (pause) Damn! Can't get a hold of it. I need something thin.

The password is IMOGERON (GERONIMO, the password from the router rotated 3 characters alphabetically to the right as hinted by [password_note](../items/password_note.md))

After the password is entered, see [PUZZLE 2 3 Finding the wifi router](../gdd.md#PUZZLE%202%203%20Finding%20the%20wifi%20router).

## Searching for a virgin mary recipe

Used at [PUZZLE 3 5 Getting fingerprints from Rebeca](../gdd.md#PUZZLE%203%205%20Getting%20fingerprints%20from%20Rebeca)

When opened for the first time:

We shortly see the dashboard, then the browser symbol is highlighted (as if touched) and the screen changes to the browser, with the search engine "Moby" opened.

She types "virgin mary recipe", gets a list of results and then opens the first one showing:

**Virgin Mary**

(Non-alcoholic version of *Bloody Mary*)

Ingredients:
- Tomato juice
- Lemon juice
- Salt
- Pepper
- Tabasco

The rest of the recipe is hidden from the player's view.

We close the [smartphone](smartphone.md).

> [marjorie](../characters/marjorie.md)
> Hm. Let's see, what we can find.

When opened again, we directly see the recipe.