# Padlock

#closeup 

A silver padlock with three vertical dials which show different colors. All dials can be turned. The [gruesome_note](../items/gruesome_note.md) hints at the solution for this, as it refers to three shapes (pyramid, sphere, cube), which reference three items: The [hall_lamp](../items/hall_lamp.md), the [bedroom_lamp](../items/bedroom_lamp.md) and the [kitchen_trash_can](../items/kitchen_trash_can.md), which are blue, green and red. Thus, the solution is *blue*, *green*, *red* from top to bottom.

The other available colors are yellow, white and black.

Once all dials have been turned correctly, the padlock opens and [bedroom_wardrobe_drawer_lower](../items/bedroom_wardrobe_drawer_lower.md) is unlocked. We exit back to [bedroom](../locations/bedroom.md)

## SFX

Turning the dials plays [turning_dials](../sfx/turning_dials.md). Unlocking plays [unlock_padlock](../sfx/unlock_padlock.md). 