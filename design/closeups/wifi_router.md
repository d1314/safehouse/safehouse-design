# Wifi Router

#closeup 

A [upper_hall_Wi-Fi_router](../items/upper_hall_wifi_router.md) sits in a free space under a wooden floor. A loose board lies next to it. There are cables going into the sides, and two antennas at the top.

On the front is a vendor logo. It has two rectangles side by side (symbolizing the [Leverkusen Rhine Bridge](https://de.wikipedia.org/wiki/Rheinbr%C3%BCcke_Leverkusen)) and the name "Levku". Under the logo sits the model name "AirComm 1155". In the lower right corner, we see LEDs showing Power, Connect, LAN and Wi-Fi. Power and Connect are static green, LAN is off and Wi-Fi is blinking irregularly.

The Wi-Fi router can be turned around. On the back is a sticky note with "Wi-Fi-PASSWORD: GERONIMO" scribbled upon.

Exit takes the player back to [upper_hall](../locations/upper_hall.md)