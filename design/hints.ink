-> HINTS_PUZZLE_1_1
== HINTS_PUZZLE_1_1 ==

{ shuffle:

    - # Key: HINTS_PUZZLE_1_1_1
      marjorie: I think, I should unpack. # tag

    - # Key: HINTS_PUZZLE_1_1_2
      marjorie: Maybe it's time to unpack.

}

->END

== HINTS_PUZZLE_1_2 ==

{ shuffle:
    
    - # Key: HINTS_PUZZLE_1_2_1
      marjorie: Damn! Where did all the parts go?

    - # Key: HINTS_PUZZLE_1_2_2
      marjorie: I need to fix my phone.

    - # Key: HINTS_PUZZLE_1_2_3
      marjorie: I'm so clumsy. Damn phone.
}


-> END

== HINTS_PUZZLE_1_3 ==

{ shuffle:

    - # Key: HINTS_PUZZLE_1_3_1
      marjorie: Mmmm. Something small and pointy.
      
    - # Key: HINTS_PUZZLE_1_3_2
      marjorie: I think, I packed something.
      
    - # Key: HINTS_PUZZLE_1_3_3
      marjorie: Let's check out the suitcase again.
    
}

-> END

== HINTS_PUZZLE_1_4 ==

{ shuffle:

    - # Key: HINTS_PUZZLE_1_4_1
      marjorie: Hm. Now what was my PIN code again?
     
    - # Key: HINTS_PUZZLE_1_4_2
      marjorie: I need to remember my PIN code.
      
}

-> END

== HINTS_PUZZLE_1_5 ==

{ shuffle:

    - marjorie: I better clean up the table.
 
    - marjorie: Let's clean the dishes.
 
    - marjorie: Let's throw away the trash.
 
    - marjorie: Maybe I should apologize to Rebeca
 
    - marjorie: Maybe I should talk to Calvin.

}

-> END
