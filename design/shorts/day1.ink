== PUZZLE_1_1 ==

# Key: DAY1_PUZZLE1_1
# Direction: shots
marjorie: Damn!

->END

== PUZZLE_1_2_1 ==

# Key: DAY1_PUZZLE1_2_1
marjorie: I think that's everything.

->END

== PUZZLE_1_2_2 ==

# Key: DAY1_PUZZLE1_2_2
marjorie: There. It's working again.

# Key: PAUSE
marjorie: ...

# Key: DAY1_PUZZLE1_2_3
marjorie: Ugh, it needs to be reset because I dropped it. There's this tiny button on the side. I need something small. Like a paper clip, needle or something.

->END

== PUZZLE_1_3 ==

# Key: DAY1_PUZZLE_1_3_1
marjorie: It's a bit too big for the hole, a paper clip would be better. 

# Key: DAY1_PUZZLE_1_3_2
# Direction: Fiddles with the hairpin
marjorie: But... yes, it works.

# Key: DAY1_PUZZLE_1_3_3
marjorie: Here we go.

# Key: PAUSE
marjorie: ...

# Key: DAY1_PUZZLE_1_3_4
marjorie: Ugh, now it wants my PIN? I always forget stuff like that.

-> END

== DAY1_CONTINUED_MORNING

# Key: DAY1_CONTINUED_MORNING_1
# Direction: Very emotional
marjorie: Mom?

# Key: DAY1_CONTINUED_MORNING_2
marjorie: Hi, Mom. Yes, we arrived.

# Key: DAY1_CONTINUED_MORNING_3
marjorie: Oh, it's okay, I guess. We're...

// We hear Rebeca from the outside
# Key: DAY1_CONTINUED_MORNING_4
rebeca: Marjorie, I have some shee...

/*
 * Rebeca has entered the door and sees her on the phone.
 * They stare at each other for a few seconds.
 */

# Key: DAY1_CONTINUED_MORNING_5
# Direction: *REALLY* pissed off, shouting
rebeca: You fucking fool!

// She rushes to Marjorie, grabs the smartphone, rips away the simcard, throws it out of the window and hides the smartphone. Marjorie starts to cry.

# Key: DAY1_CONTINUED_MORNING_6
# Direction: Shouting
rebeca: What part of witness protection did you not understand?

// Rebeca walks up and down the room

# Key: DAY1_CONTINUED_MORNING_7
# Direction: Shouting, accentuating specific words
rebeca: Not ONLY am I WASTING MY TIME AND FUCKING RISKING MY LIFE babysitting her, she goes about and brings a FUCKING CELLPHONE TO THE SAFE HOUSE.

// She turns around, pointing at Marjorie

# Key: DAY1_CONTINUED_MORNING_8
# Direction: Getting really mean
rebeca: Do you want him to just get on with it and kill you? We could all save some time in that case.

// Beat. We hear Marjorie sobbing

# Key: DAY1_CONTINUED_MORNING_9
# Direction: Shouting
rebeca: Christ!

-> END

== DAY1_NOON ==

# Key: DAY1_NOON_1
# Direction: Without looking up
calvin: Rebeca?

# Key: PAUSE
rebeca: ...

# Key: DAY1_NOON_2
# Direction: looking up
calvin: Rebeca?

# Key: DAY1_NOON_3
# Direction: slurry
rebeca: (grunts) 'm sorry, I freaked 'cause you brought your damn phone.

# Key: DAY1_NOON_4
calvin: (sighs) What Rebeca is TRYING to say, is:

# Key: DAY1_NOON_5
calvin: We're here to protect you for the next five days, Marjorie. That's our *job*.

# Key: DAY1_NOON_6
calvin: Key to that, is that nobody knows where you are. You understand?

# Key: DAY1_NOON_7
# Direction: shy
marjorie: ...Yes.

# Key: DAY1_NOON_8
calvin: I know, this whole situation is scary and pretty hard on you, but you have to trust us. If you need to talk, come to...

// Cavlin points at himself, then Rebeca. (beat) Calvin lowers his hand.

# Key: DAY1_NOON_9
calvin: Come to me. We're here for you if you need anything, okay?

// Calvin smiles at Marjorie. Marjorie shyly smiles back.

marjorie: Thank you.

// Marjorie turns to Rebeca.

marjorie: And I'm sorry. Bringing the phone really was a silly idea. I just... 

marjorie: I just miss my mom and yes, this is all very creepy for me.

rebeca: Yeah, whatever. Just try to not get killed, okay?

rebeca: At least not on my watch.

// They have finished eating.

// Marjorie rises.

marjorie: You know what? Let me handle this.

calvin: Thank you, Marjorie.

-> END