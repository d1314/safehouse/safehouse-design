# Lamp (Upper hall)

#item #upperhall 

A lamp in the [upper_hall](../locations/upper_hall.md)

- Look
	- on [PUZZLE 3 1 - Fixing the lamp](../gdd.md#PUZZLE%203%201%20-%20Fixing%20the%20lamp)
		
		> [marjorie](../characters/marjorie.md)
		> No, the bulb has the wrong size.
	- Default
		
		> [marjorie](../characters/marjorie.md)
		> It's actually a bit bright.