# Jalapeno (cut)

#item

The [jalapeno_peppers](items/jalapeno_peppers.md), cut into slices.

- Look

  > [marjorie](characters/marjorie.md)
  >
  > Those look hot. I need to watch out to not touch them and my eyes after that.

- Use with [glass](items/glass.md) (if every other recipe was added)

  > [marjorie](characters/marjorie.md)
  > Well, it's not perfect, but it's the best Virgin Mary you can find in this house.

  Add [makeshift_virgin_mary](items/makeshift_virgin_mary.md)
  Remove [cut_peppers](items/cut_peppers.md)