# Table

#item #livingroom 

A wooden, rectangular table with four chairs, which is used during meals.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> It's where we eat.
	
- Use
	
	> [marjorie](../characters/marjorie.md)
	> I can't sit right now.