# Sideboard

#item #hall

A simple sideboard with one [hall_drawer](hall_drawer.md).

There's a [tape](tape.md) on it.

- Look | Use
	
	> [marjorie](../characters/marjorie.md)
	> I guess, you usually put your car keys on that.

- Use (if [tape](tape.md) wasn't added)
	
	> [marjorie](../characters/marjorie.md)
	> Well, somebody put some tape there.
	
	Add [tape](tape.md)