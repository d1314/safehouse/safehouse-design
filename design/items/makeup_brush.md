# Makeup brush

#item #kitchen (in [kitchen_trash_can](kitchen_trash_can.md))

A standard issue, but used makeup brush. Has a very bushy brush.

- Look
  > [marjorie](characters/marjorie.md)
  > I guess, this is Rebeca's. I don't use brushes.
- Use: See [fingerprint_powder](fingerprint_powder.md)