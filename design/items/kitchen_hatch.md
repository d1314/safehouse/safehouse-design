# Hatch (Kitchen)

#item #kitchen 

A hole in the kitchen leading to the living room. Clicking on it will switch to the living room with Marjorie looking through the hatch. It's the kitchen side of [living_room_hatch](living_room_hatch.md)

- Look | Use
	
	Switch scene to [living_room](../locations/living_room.md) with [marjorie](../characters/marjorie.md) looking through the hatch.