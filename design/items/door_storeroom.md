# Door (Storeroom)

#item #kitchen 

A door to a storeroom from the kitchen. Can't be opened.

- Look | Use
	> [marjorie](../characters/marjorie.md)
	> I guess, there's some kind of store room or something.
	> It's locked and I don't think, I need something from there anyway.