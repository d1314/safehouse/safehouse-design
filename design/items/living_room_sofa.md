# Sofa

#item #livingroom 

An L-shape sofa running across the back side of the living room and into the room. There's a glass table in front of it. The glass table is inaccessible for the player.

It's where [brianna](../characters/brianna.md) and [marjorie](../characters/marjorie.md) sit during their dialogs, it's where [rebeca](../characters/rebeca.md) sits during [PUZZLE 2 2 Getting the phone back](../gdd.md#PUZZLE%202%202%20Getting%20the%20phone%20back) and [marjorie](../characters/marjorie.md) at the beginning of [DAY 3 - EVENING](../gdd.md#DAY%203%20-%20EVENING)

- Look
	
	> [marjorie](../characters/marjorie.md)
	> It looks comfier than it actually is.
	
- Use
	
	> [marjorie](../characters/marjorie.md)
	> I can't sit right now.