# Other door

#item #upperhall 

Another door in [upper_hall](../locations/upper_hall.md) which is unaccessible.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> That's Calvin's and Rebeca's room.
	
- Use
	
	> [marjorie](../characters/marjorie.md)
	> No. I have no business in there.