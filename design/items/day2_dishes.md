# Dishes (Day 2)

#item #livingroom

Plates, cutlery and mugs from a breakfast.

- Look

  > [marjorie](characters/marjorie.md)
  > I love breakfast. Let's get those plates clean now.
- If used with [kitchen_sink](items/kitchen_sink.md)

  Play [cleaning_dishes](../sfx/cleaning_dishes.md)

  > [marjorie](characters/marjorie.md)
  > (humming/whistling)

  Remove [day2_dishes](items/day2_dishes.md) from inventory
