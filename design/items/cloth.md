# Cloth

#item #kitchen  (in the [kitchen_left_drawer](kitchen_left_drawer.md))

A simple cotton cloth.

- Look

  > [marjorie](characters/marjorie.md)
  >
  > A cloth. I'm not gonna clean this place up, though.

- Use with [hall_lamp](hall_lamp.md) on Day 3 if still turned on
	
	> [marjorie](../characters/marjorie.md)
	> No, I don't want to risk being eletrocuted. I should switch it off first.
- Use with [hall_lamp](hall_lamp.md) on Day 3
	
	> [marjorie](../characters/marjorie.md)
	> Okay, that might work.
	
	Play [screwing_out_bulb](../sfx/screwing_out_bulb.md)
	
	> [marjorie](../characters/marjorie.md)
	> Got it.
	
	Add [bulb](bulb.md)