# Fingerprints (Hook)

#item 

A black fingerprint on a transparent tape.

- Look
  > [marjorie](characters/marjorie.md)
  > I've taken that fingerprint from the hook. I need another one to compare it against.
- Use with [rebeca_fingerprints](rebeca_fingerprints.md)
	See [PUZZLE 3 6 Comparing fingerprints](../gdd.md#PUZZLE%203%206%20Comparing%20fingerprints)