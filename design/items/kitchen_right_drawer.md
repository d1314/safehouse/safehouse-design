# Right drawer

#item #kitchen

A simple drawer in [kitchen_right_cupboard](kitchen_right_cupboard.md)

- Default

  > [marjorie](characters/marjorie.md)
  >
  > It's a drawer. I guess for cutlery.

- Using it

  Play [drawer_opening](../sfx/drawer_opening.md)

  > [marjorie](characters/marjorie.md)
  >
  > Full of cutlery. (beat) oh, and a magnifying glass.

  Take the [magnifying_glass](items/magnifying_glass.md).
  Play [drawer_closing](../sfx/drawer_closing.md)
