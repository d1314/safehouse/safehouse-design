# Lightswitch

#item #hall

A simple light switch next to the door. Switches [hall_lamp](hall_lamp.md) on and off.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> It controls the lamp.
- Use
	- If night
		Play [light_switch](../sfx/light_switch.md).
		Turn [hall_lamp](hall_lamp.md) on/off
	- Default

		> [marjorie](../characters/marjorie.md)
		> It's bright enough.