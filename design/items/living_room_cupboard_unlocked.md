# Cupboard (unlocked)

#item #livingroom 

This is the open state of [living_room_cupboard](living_room_cupboard.md). It's only visible on [Day 3](../gdd.md#Day%203)

- Look
	
	> [marjorie](../characters/marjorie.md)
	> Huh? I guess, somebody forgot to lock it.
	
- Use
	- If [fingerprint_powder](fingerprint_powder.md) hasn't been taken
		
		> [marjorie](../characters/marjorie.md)
		> There is police equipment in here... (pause) Nothing of interest.
		> (beat) Ah, here's a fingerprint set. There's only fingerprint powder left though.
		> I'm taking that.
		
		Add [fingerprint_powder](fingerprint_powder.md)
	- Default
		
		> [marjorie](../characters/marjorie.md)
		> There's nothing of interest anymore. Just police stuff.