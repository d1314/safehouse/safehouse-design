# Stool

#item #bedroom 

A very basic wooden stool. Ikea-style.

- Look | Use

  - On day 3 early morning when searching for a way to reach the lamp

    > [marjorie](characters/marjorie.md)
    >
    > I'm not gonna use that. It looks as if it would break just by looking at it.

  - During [PUZZLE 1 2 Assembling the phone](../gdd.md#PUZZLE%201%202%20Assembling%20the%20phone)

    > [marjorie](characters/marjorie.md)
    >
    > Ah, there's the battery.

    Add [battery](items/battery.md)
	
  - Default

    > [marjorie](characters/marjorie.md)
    >
    > Well, my back hurts by even looking at that tiny stool. I'm not gonna sit there!