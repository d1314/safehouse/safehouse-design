# Bedroom door

#item #bedroom 

The door to Marjorie's bedroom. We need an open and closed state here. The open state shows part of the [upper_hall](../locations/upper_hall.md).

- Look

  > [marjorie](characters/marjorie.md)
  > This leads to the upper hall.

- Use

  Opens/Closes the door