# Hairpin

#item #bedroom #purse 

A hair pin.

- Look

  > [marjorie](characters/marjorie.md)
  > A tiny hair pin.

- Use in [purse](../closeups/purse.md)

  > [marjorie](characters/marjorie.md)
  >
  > That may be useful.

  Add [hairpin](items/hairpin.md)

- Use with [smartphone](items/smartphone.md)
  See [PUZZLE 1 3 Resetting the phone](../gdd.md#PUZZLE%201%203%20Resetting%20the%20phone)