# Drawer

#hall #item 

A drawer in the [hall_sideboard](hall_sideboard.md). It contains [hammer](hammer.md) when opened.

- Look
	> [marjorie](../characters/marjorie.md)
	> I'd put scarves in there.
- Use 
	- if [hammer](hammer.md) wasn't taken before
		Play [drawer_opening](../sfx/drawer_opening.md)
		
		> [marjorie](../characters/marjorie.md)
		> Oh, a hammer?
		
		Add [hammer](hammer.md)
		
		Play [drawer_closing](../sfx/drawer_closing.md)
		
	- Default
		> [marjorie](../characters/marjorie.md)
		> Nothing in there anymore.