# Cupboard

#item #livingroom 

A cupboard with a lockable door. This is just the closed state of [living_room_cupboard_unlocked](living_room_cupboard_unlocked.md) and is visible on every day but [Day 3](../gdd.md#Day%203).

- Look
	
	> [marjorie](../characters/marjorie.md)
	> It's a cupboard that Calvin and Rebeca uses. It's locked.
	
- Use
	
	> [marjorie](../characters/marjorie.md)
	> It's locked.