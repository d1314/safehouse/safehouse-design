# Makeshift virgin mary

#item

A kitchen glass with a red liquid in it.

- Look
  > [marjorie](characters/marjorie.md)
  > I've used up all my bartender skills for that.
- Use
	- with [rebeca](../characters/rebeca.md)
		
		See [PUZZLE 3 5 Getting fingerprints from Rebeca](../gdd.md#PUZZLE%203%205%20Getting%20fingerprints%20from%20Rebeca)
		
		Add [used_glass](used_glass.md)
		Remove [makeshift_virgin_mary](makeshift_virgin_mary.md)