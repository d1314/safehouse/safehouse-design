# Dishes (Day 1)

#item #livingroom

Plates and cutlery.

- Look

  > [marjorie](characters/marjorie.md)
  > All ate up. Let's quickly clean it up.

- Use with [kitchen_sink](items/kitchen_sink.md)

  Play [cleaning_dishes](../sfx/cleaning_dishes.md)

  > [marjorie](characters/marjorie.md)
  > There you go. Everything's cleaned up.

  Remove [day1_dishes](items/day1_dishes.md) from inventory.
