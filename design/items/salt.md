# Salt

#item #livingroom 

A salt shaker with an S on the top.

- Look
  > [marjorie](characters/marjorie.md)
  > There's only little salt left in the shaker.
- Use with [glass](items/glass.md)
  > [marjorie](characters/marjorie.md)
  > I should add the tomatoes first.
- Use with [glass_with_tomatoes](items/glass_with_tomatoes.md) (if already done)
  > [marjorie](characters/marjorie.md)
  > I think, that's enough salt
- Use with [glass_with_tomatoes](items/glass_with_tomatoes.md)
  Play [pouring_salt_pepper](../sfx/pouring_salt_pepper.md)
  > [marjorie](characters/marjorie.md)
  > Salted tomatoe juice.