# Makeshift fingerprint kit

#item

A combination of the [makeup_brush](items/makeup_brush.md), [fingerprint_powder](items/fingerprint_powder.md) and [tape](items/tape.md).

- Look

  > [marjorie](../characters/marjorie.md)
  > It should be enough to lift fingerprints and compare them.

- Use
  - With [bedroom_hook](bedroom_hook.md) on [PUZZLE 3 3 Getting the fingerprint kit](../gdd.md#PUZZLE%203%203%20Getting%20the%20fingerprint%20kit)
  	See [PUZZLE 3 3 Getting the fingerprint kit](../gdd.md#PUZZLE%203%203%20Getting%20the%20fingerprint%20kit)
  - With [bedroom_hook](items/bedroom_hook.md) on [PUZZLE 3 4 Lifting fingerprints](../gdd.md#PUZZLE%203%204%20Lifting%20fingerprints)

    > [marjorie](characters/marjorie.md)
    >
    > Okay... here we go. (pause) Yes! I got a print. Now, I need to compare it with another one.

    Add [fingerprint_from_hook](items/fingerprint_from_hook.md) to inventory

	- With [used_glass](used_glass.md)
	
		 > [marjorie](../characters/marjorie.md)
		 > Time to get your (pause) fingerprints (pause) bitch.
     
		Add [rebeca_fingerprints](rebeca_fingerprints.md)
		Remove [makeshift_fingerprint_kit](makeshift_fingerprint_kit.md)
		Remove [used_glass](used_glass.md)