# Padlock

#item #bedroom on [bedroom_wardrobe_drawer_lower](bedroom_wardrobe_drawer_lower.md)

- Look
  > [marjorie](characters/marjorie.md)
  > Strange. A padlock on a drawer.

- Use
  Show [CLOSEUP_PADLOCK](../closeups/padlock.md)
  