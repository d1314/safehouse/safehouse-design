# Bed

#item #bedroom

A very basic bed with a wooden frame, a white mattress, white pillow and a red blanket.

- Look
  - On [Day 1](../gdd.md#Day%201)

    > [marjorie](../characters/marjorie.md)
    > It's my bed, I guess.

  - After [Day 1](../gdd.md#Day%201)

    > [marjorie](characters/marjorie.md)
    >
    > My bed. The mattress is too soft, it hurts my back.

  - During [PUZZLE 1 2 Assembling the phone](../gdd.md#PUZZLE%201%202%20Assembling%20the%20phone)

    > [marjorie](../characters/marjorie.md)
    > Oh, there's the phone.

    Add [smartphone_incomplete](items/smartphone_incomplete.md)

- Use

  > [marjorie](characters/marjorie.md)
  > I'm not sleepy right now.
