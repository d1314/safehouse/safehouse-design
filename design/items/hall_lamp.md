# Lamp

#item #hall

A three-cornered wall lamp. The light is only seen in night scenes and illuminates the room and accentuates the wall and the ceiling over it. The lamp itself is tinted blue.

- Default

  > [marjorie](characters/marjorie.md)
  >
  > That actually looks quite comfy.

- On day 3, early morning

  > [marjorie](characters/marjorie.md)
  >
  > Ah. That lamps seems to have the bulb I need. But it's too hot!

- On day 3, early morning when used with cloth
  Play [screwing_out_bulb](../sfx/screwing_out_bulb.md)

  > [marjorie](characters/marjorie.md)
  >
  > There we go.

  Add [bulb](items/bulb.md)
