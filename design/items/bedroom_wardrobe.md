# Wardrobe

#item #bedroom

A very basic wooden wardrobe, just two doors and [bedroom_wardrobe_drawer_upper](items/bedroom_wardrobe_drawer_upper.md) and [bedroom_wardrobe_drawer_lower](items/bedroom_wardrobe_drawer_lower.md) under that. The lower drawer has a padlock attached to it.

- Look

  - During [PUZZLE 1 2 Assembling the phone](../gdd.md#PUZZLE%201%202%20Assembling%20the%20phone)

	  > [marjorie](characters/marjorie.md)
	  >
	  > There's that tiny phone card.

	  Add [simcard](items/simcard.md)

  - Default

    > [marjorie](characters/marjorie.md)
    >
    > A simple wardrobe for my clothes. I'll only be here for a few days.
