# Fingerprint powder

#item 

A black powder.

- Look

  > [marjorie](characters/marjorie.md)
  >
  > Some black powder I got from that fingerprint set.

- Use
  - With [tape](items/tape.md) or [makeup_brush](items/makeup_brush.md) while the other one is missing
    > [marjorie](characters/marjorie.md)
    > Hm. I still need something else to take fingerprints.
  - With [tape](items/tape.md) or [makeup_brush](items/makeup_brush.md)

    > [marjorie](characters/marjorie.md)
    > Hmmmm! Yes. Together with the tape and the brush, this will help me take fingerprints

    Add [makeshift_fingerprint_kit](items/makeshift_fingerprint_kit.md)
    Remove [fingerprint_powder](items/fingerprint_powder.md), [tape](items/tape.md) and [makeup_brush](items/makeup_brush.md)