# Wifi router

#item #upperhall in [wifi_router](../closeups/wifi_router.md)

A WIFI router, fantasy vendor and model. There are cables going in and two antennas, some blinking lights.

The WIFI router can be turned around. On the back is a sticky note with "WIFI-PASSWORD: GERONIMO" scribbled upon

- Look
	- Front side
		
		> [marjorie](../characters/marjorie.md)
		> I'm no expert, but I guess this is the Wi-Fi router for the house.
		
	- Back side
		
		> [marjorie](../characters/marjorie.md)
		> The note says WiFi-Password: GERONIMO
- Use
	Play [wifi_router_turned_around](../sfx/wifi_router_turned_around.md)
	Switch front side/back side