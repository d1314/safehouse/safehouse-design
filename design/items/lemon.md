# Lemon

#item #livingroom (in [bowl](bowl.md)) 

A fresh, yellow lemon.

- Look
  > [marjorie](characters/marjorie.md)
  > I don't like eating lemons as is.

- Use with [glass](items/glass.md)  (if googled for recipe)
  > [marjorie](characters/marjorie.md)
  > I should cut it to make it easier to squeeze.

- Use with [knife](items/knife.md) (if googled for recipe): See [knife](items/knife.md)