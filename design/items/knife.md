# Knife

#item #kitchen in the [kitchen_right_drawer](kitchen_right_drawer.md)

A sharp kitchen knife.

- Look
  
  > [marjorie](characters/marjorie.md)
  > Looks sharp. I should be cautious.
- Use
	- With [upper_hall_loose_board](upper_hall_loose_board.md)
		
		> [marjorie](../characters/marjorie.md)
		> That might work. Let's see...
		
		Play [prying_board](../sfx/prying_board.md)
		
		> [marjorie](../characters/marjorie.md)
		> Yes!
		
		Show [wifi_router](../closeups/wifi_router.md)
	- With [lemon](items/lemon.md) (if googled for recipe)

	  Play [cutting_lemon](../sfx/cutting_lemon.md)

	  > [marjorie](characters/marjorie.md)
	  > Shit. I got some juice in my eye.

	  Remove [lemon](items/lemon.md)
	  Add [half_lemon](items/half_lemon.md)

	- With [jalapeno_peppers](items/jalapeno_peppers.md) (if googled for recipe)

	  Play [slicing_peppers](../sfx/slicing_peppers.md)

	  Add [cut_peppers](items/cut_peppers.md)
	  Remove [knife](items/knife.md) (if also cut lemon)
