# Leftovers (Day 1)

#item #livingroom 

Boxes from a chinese restaurant.

- Look

> [marjorie](../characters/marjorie.md)
> Ugh. I can't stand the smell anymore, now that I'm done with it. Better throw it away.

- Use with [kitchen_trash_can](items/kitchen_trash_can.md)

  Play [throwing_into_the_trash](../sfx/throwing_into_the_trash.md)

  Remove [day1_leftovers](items/day1_leftovers.md) from inventory.