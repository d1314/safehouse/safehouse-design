# Pocket

#item #livingroom 

[Rebeca](../characters/rebeca.md#Rebeca)'s pocket with [Marjorie](../characters/marjorie.md#Marjorie)'s smartphone looking out. Visible during [PUZZLE 2 2 Getting the phone back](../gdd.md#PUZZLE%202%202%20Getting%20the%20phone%20back)

- Look
	
	> [marjorie](../characters/marjorie.md)
	> That's my phone!
	
- Use
	- from [living_room](../locations/living_room.md)
		
		> [marjorie](../characters/marjorie.md)
		> Nah. She wouldn't let me.
	- from [living_room_hatch](living_room_hatch.md) if Rebeca leans to the left and reads the file
		
		> [marjorie](../characters/marjorie.md)
		> (whispers) Careful...
		
		Trigger animation where [Marjorie](../characters/marjorie.md#Marjorie) fetches the phone.
		
		> [marjorie](../characters/marjorie.md)
		> (whispers) Yes!
		
		Add [smartphone](smartphone.md)
		Disable [pocket](pocket.md)
	- from [living_room_hatch](living_room_hatch.md)
		Marjorie reaches down, but immediately pulls back her hand.
		
		> [marjorie](../characters/marjorie.md)
		> (whispers) Phew, that was close. I should be more careful.