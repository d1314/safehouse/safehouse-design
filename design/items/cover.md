# Cover

#item #bedroom (under the [bedroom_nightstand](bedroom_nightstand.md))

The back cover of the [smartphone](items/smartphone.md).

- Look

  > [marjorie](characters/marjorie.md)
  >
  > It's the back of my smartphone.

- Use with [smartphone_incomplete](smartphone_incomplete.md) if not all parts have been added
	
	> [marjorie](../characters/marjorie.md)
	> I should add it as the last part.
- Use with [smartphone_incomplete](smartphone_incomplete.md)
	
	Play [inserting_object](../sfx/inserting_object.md)
	
	> [marjorie](../characters/marjorie.md)
	> Okay, everything's back together.
	
	Remove [smartphone_incomplete](smartphone_incomplete.md)
	Remove [cover](cover.md)
	Add [smartphone](smartphone.md)
	
	See end of [PUZZLE 1 2 Assembling the phone](../gdd.md#PUZZLE%201%202%20Assembling%20the%20phone) 