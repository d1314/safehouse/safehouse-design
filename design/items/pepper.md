# Pepper

#item #livingroom (on the [living_room_table](living_room_table.md))

Pepper in a pepper shaker with a P on the top.

- Look
  > [marjorie](characters/marjorie.md)
  > Careful not to sneeze.
- Use
	- With [glass](items/glass.md)  (if googled for recipe)
	  > [marjorie](characters/marjorie.md)
	  > I should add the tomatoes first
	- With [glass_with_tomatoes](items/glass_with_tomatoes.md) (if done already)
	  > [marjorie](characters/marjorie.md)
	  > That's enough pepper I guess.
	- With [glass_with_tomatoes](items/glass_with_tomatoes.md)
	  Play [pouring_salt_pepper](../sfx/pouring_salt_pepper.md)
	  > [marjorie](characters/marjorie.md)
	  > Peppered tomatoe juice.