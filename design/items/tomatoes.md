# Tomatoes

#item #kitchen (in the [kitchen_fridge](kitchen_fridge.md))

- Look

  > [marjorie](characters/marjorie.md)
  > Fresh and red. They look pretty tasty.
- Use
	- with [hammer](hammer.md): See [hammer](hammer.md)
	- with [glass](items/glass.md)  (if googled for recipe)
	  > [marjorie](characters/marjorie.md)
	  > I should mash them first.
