# Cupboard (right)

#item #kitchen

A simple cupboard with two doors and a [kitchen_left_drawer](kitchen_left_drawer.md) and [kitchen_right_drawer](kitchen_right_drawer.md) We need the cupboard doors opened and closed

- Look
	
	> [marjorie](../characters/marjorie.md)
	> The dishes are stored in there. Plates, mugs, glasses.
	
- Use
	
	- on [PUZZLE 3 5 Getting fingerprints from Rebeca](../gdd.md#PUZZLE%203%205%20Getting%20fingerprints%20from%20Rebeca)
		
		Play [cupboard_door_opening](../sfx/cupboard_door_opening.md)
		
		> [marjorie](../characters/marjorie.md)
		> Let's take this.
		
		Add [glass](glass.md)
		
		Play [cupboard_door_closing](../sfx/cupboard_door_closing.md)
	- Default
		
		> [marjorie](../characters/marjorie.md)
		> I don't need anything from there right now.