# Door (Kitchen)

#livingroom #item 

The door to the kitchen.

We need an open and closed state. The default state is closed.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> The kitchen is behind that door.
- Use
	
	Open/close door