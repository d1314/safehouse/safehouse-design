# Stove

#kitchen #item 

A stove, which is never used.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> We have a stove, but we only use it sparsely.
	
- Use
	
	> [marjorie](../characters/marjorie.md)
	> I'm not hungry now.