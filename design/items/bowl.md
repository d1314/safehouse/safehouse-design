# Bowl

#item #livingroom (on the [living_room_table](living_room_table.md))

A wooden bowl, used for fruits.

- Look
	- If lemon was taken
		
		> [marjorie](../characters/marjorie.md)
		> It's a fruit bowl. It's empty.
	- If lemon wasn't taken
		
		> [marjorie](../characters/marjorie.md)
		> It's a fruit bowl. There's only a lemon left.
- Use (If lemon wasn't taken)
	Add [lemon](lemon.md)
