# Smartphone (incomplete)

#item #bedroom (under [bedroom_bed](bedroom_bed.md))

The incomplete smartphone base. Requires [battery](items/battery.md), [cover](items/cover.md), [simcard](items/simcard.md)

- If examined

  > [marjorie](characters/marjorie.md)
  >
  > (grunt) I'm so clumsy. I hope, it isn't broken.

