# Smartphone

#item

Smartphone (rather cheap), completed from [smartphone_incomplete](smartphone_incomplete.md), [battery](battery.md), [simcard](simcard.md) and [cover](cover.md)

- Look | Use
  - If not reset.

    > [marjorie](../characters/marjorie.md)
    > There's a message saying, the phone needs to be reset. (grunts)

  - If reset:

    > [marjorie](characters/marjorie.md)
    >
    > It asks for my PIN now.

    Show [smartphone](../closeups/smartphone.md)
	
  - Default (until [PUZZLE 2 3 Finding the wifi router](../gdd.md#PUZZLE%202%203%20Finding%20the%20wifi%20router)) and on [PUZZLE 3 5 Getting fingerprints from Rebeca](../gdd.md#PUZZLE%203%205%20Getting%20fingerprints%20from%20Rebeca):
     Show [smartphone](../closeups/smartphone.md)
	 
  - Default (after [PUZZLE 2 3 Finding the wifi router](../gdd.md#PUZZLE%202%203%20Finding%20the%20wifi%20router)):
     > [marjorie](../characters/marjorie.md)
     > It's my phone. I don't need it now.
