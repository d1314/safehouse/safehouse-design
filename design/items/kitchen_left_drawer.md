# Left drawer

#item #kitchen 

A drawer in [kitchen_right_cupboard](kitchen_right_cupboard.md).

- Look
	
	> [marjorie](../characters/marjorie.md)
	> I wonder what's in there.
- Use
	- If [cloth](cloth.md) has not been taken
		
		Play [drawer_opening](../sfx/drawer_opening.md)
		
		> [marjorie](../characters/marjorie.md)
		> There's a cloth inside. I'll take it. I won't clean this place, though.
		
		Add [cloth](cloth.md)
		
		Play [drawer_closing](../sfx/drawer_closing.md)
	- Default
		
		> [marjorie](../characters/marjorie.md)
		> There's nothing of interest in there anymore.