# Loose board

#item #upperhall 

A loose wooden board on the floor of [upper_hall](../locations/upper_hall.md). Only active from [PUZZLE 2 3 Finding the wifi router](../gdd.md#PUZZLE%202%203%20Finding%20the%20wifi%20router) on.

- Look
	- if not used with [knife](knife.md)
		
		> [marjorie](../characters/marjorie.md)
		> It's loose, but I can't get a grip.
	- If used with [knife](knife.md)
		
		Show [wifi_router](../closeups/wifi_router.md)