# Front door

#item #hall 

The front door of the house. It's partly transparent and shows the exterior. There are windows next to it showing more of the outside. The exterior is sunny and welcoming in day scenes, and eerie in the night scenes.

- Look

  > [marjorie](characters/marjorie.md)
  > It's the front door.

- Use
  - Default
    > [marjorie](characters/marjorie.md)
    >
    > I can't leave now. I'm in danger outside.
  - On day 4, early morning
    > [marjorie](characters/marjorie.md)
    >
    > (scared, hectic) Shit! It's locked. I can't get out.