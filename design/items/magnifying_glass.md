# Magnifying glass

#item #kitchen (in [kitchen_right_drawer](kitchen_right_drawer.md))

A magnifying glass as a reading aid. Rectangular blue frame with a round magnifying glass inset.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> It's a magnifying glass. I guess it's used as a reading aid.
	
- Use
	- With [tiny_thing](tiny_thing.md)
		
		See [PUZZLE 3 2 - Inspecting the hook](../gdd.md#PUZZLE%203%202%20-%20Inspecting%20the%20hook)

	- With [tiny_thing](tiny_thing.md) (again)

	  > [marjorie](characters/marjorie.md)
	  >
	  > Hemp fibres. From the noose. They give me the creeps.