# Leftovers (Day 2)

#item #livingroom 

Toast cuts, bacon leftovers, mashed eggs.

- Look

  > [marjorie](characters/marjorie.md)
  > Let's put this into the trash.

- Used with [kitchen_trash_can](items/kitchen_trash_can.md)

  Play [throwing_into_the_trash](../sfx/throwing_into_the_trash.md)

  Remove [day2_leftovers](items/day2_leftovers.md) from inventory