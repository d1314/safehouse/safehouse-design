# Bulb (working)

#item #bedroom 

A working lightbulb.

- Look
  > [marjorie](characters/marjorie.md)
  > That light bulb seems to be working.

- Use
	- With [bedroom_lamp](items/bedroom_lamp.md)

	  see [PUZZLE 3 1 - Fixing the lamp](../gdd.md#PUZZLE%203%201%20-%20Fixing%20the%20lamp) 