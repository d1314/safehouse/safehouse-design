# Pin note

#item #purse 

A note with hints for the pin.

- Look/Use

  > [marjorie](characters/marjorie.md)
  >
  > Oh, that's my secret note. Yeah, I know you shouldn't write your passwords down, but my brain's a sieve. I wrote down
  >
  > 4872
  >
  > RUBE
  >
  > caTerp1llar

  Add [password_note](../items/password_note.md)
  Close back to [bedroom](../locations/bedroom.md)