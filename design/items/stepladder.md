# Stepladder

#item #kitchen (in the [kitchen_left_cupboard](kitchen_left_cupboard.md))

A small, sturdy stepladder made from aluminium.

- Look
	
	> [marjorie](../music/marjorie.md)
	> This looks sturdy enough.
	
- Use with [bedroom_lamp](bedroom_lamp.md)
	
	> [marjorie](../characters/marjorie.md)
	> That should help getting up there.
	
	Marjorie climbs up and unscrews the broken bulb
	
	Remove [stepladder](stepladder.md)
	Add [broken_bulb](broken_bulb.md)