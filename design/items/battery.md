# Battery

#item #bedroom (under the [bedroom_stool](bedroom_stool.md))

A smartphone battery from a phone where you can actually switch batteries.

- Look

  > [marjorie](characters/marjorie.md)
  >
  > It's the battery from my phone.

- Use with [smartphone_incomplete](smartphone_incomplete.md)
	
	> [marjorie](../music/marjorie.md)
	> This... should go... there. Yes.
	
	Play [inserting_object](../sfx/inserting_object.md)
	
	Remove [battery](battery.md)