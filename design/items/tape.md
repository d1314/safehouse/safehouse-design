# Tape

#item #hall (on the [hall_sideboard](hall_sideboard.md))

A transparent, adhesive tape.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> I wonder, if it's still sticky
	
- Use: see [fingerprint_powder](fingerprint_powder.md)