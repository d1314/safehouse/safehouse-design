# SIM-Card

#item #bedroom (under the [bedroom_wardrobe](bedroom_wardrobe.md))

SIM card

- Look

	> [marjorie](characters/marjorie.md)
	>
	> That's the tiny card I need to call somebody.

- Use with [smartphone_incomplete](smartphone_incomplete.md)
	
	Play [inserting_object](../sfx/inserting_object.md)
	
	> [marjorie](../characters/marjorie.md)
	> There we go.
	
	Remove [simcard](simcard.md)