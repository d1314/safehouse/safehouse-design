# Hammer

#item #hall (in the [hall_drawer](hall_drawer.md))

A standard hammer.

- Look
  > [marjorie](characters/marjorie.md)
  > I'm not a DIY-girl. I'd hammer my thumb with this one certainly.
- Use with [tomatoes](items/tomatoes.md) (if googled for recipe)

  > [marjorie](characters/marjorie.md)
  > That's gonna be a mess, but...

  Play [../sfx/hammer_mash_tomatoes](../sfx/hammer_mash_tomatoes.md)

  > [marjorie](characters/marjorie.md)
  > Here we go.

  Remove [tomatoes](items/tomatoes.md)
  Add [mashed_tomatoes](items/mashed_tomatoes.md)