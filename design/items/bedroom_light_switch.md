# Light switch

#item #bedroom

A common light switch, clearly visible beside the door.

- Look

  > [marjorie](characters/marjorie.md)
  > The light switch for my bedroom.

- Use

  - Default

    > [marjorie](characters/marjorie.md)
    >
    > I think, the light's fine as it is.

  - On day 3 early morning when the bulb's dead

    Play [broken_light_switch](../sfx/broken_light_switch.md)

    > [marjorie](characters/marjorie.md)
    >
    > Nothing. The bulb is dead.
