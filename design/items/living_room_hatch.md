# Hatch (Living room)

#item #livingroom 

It's the living room side of [kitchen_hatch](kitchen_hatch.md), but the player can't reach it.

- Look
	- If not looking through
		
		> [marjorie](../characters/marjorie.md)
		> That's the hatch to the kitchen.
	- Default
		
		Switch back to the [kitchen](../locations/kitchen.md)

- Use
	
	Switch back to [kitchen](../locations/kitchen.md)