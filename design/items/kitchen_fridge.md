# Fridge

#item #kitchen 

A silver american-style fridge. We need an open and closed state. When opened, the player can't see inside.

- Look
	
	> [marjorie](../characters/marjorie.md)
	> It's the fridge.
- Use
	- While [PUZZLE 3 5 Getting fingerprints from Rebeca](../gdd.md#PUZZLE%203%205%20Getting%20fingerprints%20from%20Rebeca) after googling for the recipe
		
		Play [fridge_opening](../sfx/fridge_opening.md)
		
		> [marjorie](../characters/marjorie.md)
		> There are tomatoes here.
		
		Add [tomatoes](tomatoes.md)
		
		> [marjorie](../characters/marjorie.md)
		> Oh, and Jalapeños! I could use them instead of Tabasco.
		
		Add [jalapeno_peppers](jalapeno_peppers.md)
		
		Play [fridge_closing](../sfx/fridge_closing.md)
	- Default
		
		> [marjorie](../characters/marjorie.md)
		> I'm not hungry right now.