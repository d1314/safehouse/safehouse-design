# Nightstand

#item #bedroom

A very basic nightstand. Just a wooden block with a drawer.

- Look
  - During [PUZZLE 1 2 Assembling the phone](../gdd.md#PUZZLE%201%202%20Assembling%20the%20phone)

    > [marjorie](characters/marjorie.md)
    >
    > Here's the cover.

    Add [cover](items/cover.md)
  - Default

    > [marjorie](characters/marjorie.md)
    >
    > A nightstand. There's nothing interesting in the drawer.

- Use

  Play [drawer_opening](../sfx/drawer_opening.md)

  > [marjorie](characters/marjorie.md)
  > Nope. Nothing.

  Play [drawer_closing](../sfx/drawer_closing.md)
