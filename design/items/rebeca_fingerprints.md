# Fingerprints (Rebeca)

#item 

Fingerprints of [rebeca](../characters/rebeca.md), taken from the [makeshift_virgin_mary](makeshift_virgin_mary.md).

- Look
	
	> [marjorie](../music/marjorie.md)
	> These are Rebeca's.
	
- Use with [fingerprint_from_hook](fingerprint_from_hook.md). See [fingerprint_from_hook](fingerprint_from_hook.md)