# Tomatoes (mashed)

#item

A red pile of former tomatoes, mashed with [hammer](items/hammer.md).

- Look

  > [marjorie](characters/marjorie.md)
  > They're mashed, but still look pretty tasty

- Use with [glass](items/glass.md)  (if googled for recipe)

  Play [filling_tomatoes](../sfx/filling_tomatoes.md)

  Remove [mashed_tomatoes](mashed_tomatoes.md)
  Remove [glass](glass.md)
  Add [glass_with_tomatoes](glass_with_tomatoes.md)