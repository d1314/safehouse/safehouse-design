# Glass

#item #kitchen (in the [kitchen_right_cupboard](kitchen_right_cupboard.md))

A common basic and cheap drinking glass. Is the base for mixing the ingredients for the [makeshift_virgin_mary](items/makeshift_virgin_mary.md)

- Look during [PUZZLE 3 5 Getting fingerprints from Rebeca](../gdd.md#PUZZLE%203%205%20Getting%20fingerprints%20from%20Rebeca)
  > [marjorie](characters/marjorie.md)
  >
  > I can mix something in there.
- Look
  > [marjorie](../characters/marjorie.md)
  > I'm not thirsty right now.
