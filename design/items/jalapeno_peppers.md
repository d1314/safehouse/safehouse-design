# Jalapeño Peppers

#item #kitchen (in the [kitchen_fridge](kitchen_fridge.md))

Three red jalapeño peppers.

- Look

  > [marjorie](characters/marjorie.md)
  >
  > I'm not a fan of hot food.

- Use with [glass](items/glass.md) (if googled for recipe)
  > [marjorie](characters/marjorie.md)
  > I should add the tomatoes first.

- Use with [glass_with_tomatoes](items/glass_with_tomatoes.md) (if googled for recipe)
  > [marjorie](characters/marjorie.md)
  > I have to slice it first to extract the seeds.

- Use with [knife](items/knife.md)  (if googled for recipe)
  See [knife](items/knife.md)