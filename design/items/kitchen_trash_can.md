# Trash can

#item #kitchen 

A cubic, red issue trash can, that can be opened and closed using a foot pedal.

- Look | Use
	- On [DAY 2 - MORNING](../gdd.md#DAY%202%20-%20MORNING) if [password_note](password_note.md) wasn't added
		
		Play [trashcan_opens](../sfx/trashcan_opens.md)
		
		> [marjorie](../characters/marjorie.md)
		> What's that?
		
		Add [password_note](password_note.md)
		
		Play [trashcan_closes](../sfx/trashcan_closes.md)
	- Default
		
		Play [trashcan_opens](../sfx/trashcan_opens.md)
		
		> [marjorie](../characters/marjorie.md)
		> Nothing in there but trash.
		
		Play [trashcan_closes](../sfx/trashcan_closes.md)
