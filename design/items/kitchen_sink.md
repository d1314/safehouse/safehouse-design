# Sink

#item #kitchen 

A standard kitchen sink. If used with [day1_dishes](items/day1_dishes.md) or [day2_dishes](items/day2_dishes.md), it triggers a cleaning animation.

- Look | Use
	
	> [marjorie](../characters/marjorie.md)
	> There's no dishwasher here, so we clean our dishes in there.
	
