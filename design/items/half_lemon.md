# Lemon (half)

#item

The [lemon](items/lemon.md), cut in half.

- Look

  > [marjorie](characters/marjorie.md)
  > I've cut the lemon in half.

- Use with [glass](items/glass.md) (if googled for recipe)

  > [marjorie](characters/marjorie.md)
  > I should add the tomatoes first.

- Use with [glass_with_tomatoes](glass_with_tomatoes.md)
	> [marjorie](../characters/marjorie.md)
	> Squeezing it.
	
	Remove [half_lemon](half_lemon.md)