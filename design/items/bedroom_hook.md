# Hook

#item #bedroom (visible in [bedroom_lamp](../closeups/bedroom_lamp.md))

A sturdy, metallic hook. It looks arcane and creepy under the lamp. On the hook we see [tiny_thing](tiny_thing.md).

- Look

  > [marjorie](characters/marjorie.md)
  > Creepy.

- Use
  > [marjorie](characters/marjorie.md)
  > (creeped out) No thank you.
