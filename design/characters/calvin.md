# Calvin

#character

Calvin Andrews, a police officer

Calvin is one of the two detectives that ought to protect [marjorie](marjorie.md) in the safe house. However, he's actually also on [Igino](igino.md#Igino)'s payroll and staged the suicide of [Jacob](jacob.md#Jacob) eighteen years ago. He tries to scare and eventually kill [Marjorie](marjorie.md#Marjorie) (which is only revealed on the last day).

Superficially, he's a nice and caring man in his 50s. A father-figure type. This is the (visual) progression that he goes through: Starting as nice and trustworthy and becoming mean and ruthless.

- Age: 57
- Gender: cis male
- Height: 180cm
- Weight: 90kg
- Eyes: green
- Hair: sparse, black/gray
- Skin tone: gray
- Clothes:
  - Worn-out brown suit, light gray tie
  - Black shoes
- Voice:
	- Soft / Hard
	- Optimistic
	- Deep to middle