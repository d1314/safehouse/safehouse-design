# Brianna

#character

Brianna Allison, district attorney

*Briana* is the district attorney in the case where [marjorie](marjorie.md) is the key witness. She's a tough dog in the courtroom, but caring and cautious when she speaks to [marjorie](marjorie.md).

She visits the house each day to prepare [marjorie](marjorie.md) for her testimony.

She doesn't have much character progression. Her character is used to tell the backstory of the murder through her talks with [marjorie](marjorie.md), and as the last safe resort and trust in the juridical system in the last scene.

After law school, she had a rough start with several failed cases and developed an alcohol addiction, which she eventually managed and through the years and different employers she finally built up a reputation as a fair, but strong attorney. She's sober now and doesn't drink alcohol anymore.

- Age: 42
- Gender: cis female or non-binary
- Height: 173 cm
- Weight: 80 kg
- Eyes: gray blue
- Hair: shoulder long, red
- Skin tone: apricot
- Clothes:
  - Beige skirt suit
  - Black high heels
- Voice:
  - Correct
  - Direct
  - Well-chosen
  - Middle

## Animations

- [brianna_sitting](../animations/brianna_sitting.md)
- [brianna_talking](brianna_talking.md)