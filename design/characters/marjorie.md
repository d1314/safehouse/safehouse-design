# Marjorie

#character

Marjorie Tailor, the main character

Marjorie is a black woman, aged 34. At the beginning of the game, she is calm, shy and insecure. Through the events in the game, she becomes confident and strong.

Because [rebeca](rebeca.md) is very rough to her, she begins to suspect that she tries to scare her, but ultimatively, it's [calvin](calvin.md) that tries to attack her. This gets settled in the very last scene.

Throughout the game, she is scared (her fears growing with the events).

- Age: 34
- Gender: cis female
- Height: 165 cm
- Weight: 120 kg
- Eyes: brown
- Hair: Short, black
- Skin tone: brown
- Clothes:
  - Blue short dress
  - Pink shoes
- Voice:
  - soft
  - warm
  - low to middle
  - emotional
