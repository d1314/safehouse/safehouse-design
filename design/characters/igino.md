# Igino

#character

Igino Varone, mob boss

Igino is the head of the local mob. He's ruthless, mean, egocentric, and has a god complex.

[marjorie](marjorie.md) witnessed him shooting a shop owner in cold blood.

He's only briefly seen in the game in a nightmare scene, but he's the glooming shadow in the background throughout the game.

- Age: 68
- Gender: cis male
- Height: 174 cm
- Weight: 110 kg
- Eyes: brown
- Hair: short, black
- Skin tone: olive gray
- Clothes:
  - expensive, white suit
  - brown leather boots

