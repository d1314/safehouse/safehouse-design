# Jacob

#character

Jacob Grimes, former witness (deceased)

Jacob has been a witness of another murder by [igino](igino.md), when he killed the opposing drug called Derek Cole eighteen years ago. He was found strangled while being in witness protection. The death was labeled suicide, although it was staged by [calvin](calvin.md).

[calvin](calvin.md) weaves in the story of Jacob to scare [marjorie](marjorie.md), leaving scary notes and staging scary events. Jacob isn't actually seen in the game, but a picture of him could maybe be inserted in a report that [marjorie](marjorie.md) finds online.

- Age: 16
- Gender: cis male
- Height: 160cm
- Weight: 50kg
- Eyes: blue
- Hair: blonde
- Skin tone: apricot
- Clothes:
  - Black shirt, jeans
  - sneakers