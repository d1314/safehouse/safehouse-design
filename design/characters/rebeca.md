# Rebeca

#character

Rebeca Pliego, a police officer

Rebeca is one of the two police officers that ought to protect [marjorie](marjorie.md) in the safe house.

She's young, rough and harsh and she's not really happy spending five days "babysitting" [marjorie](marjorie.md), as she puts it. She comes from a family tradition of police officers, and her mother and grandfather were highly renowned police officers.

After the event with the noose she gets suspcious of [calvin](calvin.md), and investigates behind his back. This leads to a standoff between them on day four. This is only hinted to the player.

Since she finds about about [calvin](calvin.md)'s motives, she is injured by him on the fifth day.

- Age: 28
- Gender: cis female or non-binary
- Height: 170 cm
- Weight: 65 kg
- Eyes: green
- Hair: long (pony tail), brown
- Skin tone: light brown
- Clothes:
  - black suit (looking "correct"), no tie
  - black shoes
- Voice:
  - harsh
  - middle to high
  - hard
