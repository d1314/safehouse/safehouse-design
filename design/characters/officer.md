# Unnamed officer

The officer has no visual representation and is only audible in the introduction sequence.

He's a very unpleasant guy as he's driving [rebeca](../characters/rebeca.md) and [marjorie](../characters/marjorie.md) to the safehouse with slurs of sexism and racism, but gets talked down by [rebeca](../characters/rebeca.md) through the course of the introduction.

The character is used as a counter weight to [rebeca](../characters/rebeca.md).

His name is Frank.