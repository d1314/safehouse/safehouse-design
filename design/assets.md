# Assets

The following assets need to be produced

## Items

These items are either used in the inventory or a closeup.

#todo

- [x] [battery](items/battery.md)
- [x] [bras](items/bras.md)
- [x] [broken_bulb](items/broken_bulb.md)
- [x] [bulb](items/bulb.md)
- [x] [cloth](items/cloth.md)
- [x] [credit_card](items/credit_card.md)
- [x] [cover](items/cover.md)
- [x] [cut_peppers](items/cut_peppers.md)
- [x] [day1_dishes](items/day1_dishes.md)
- [x] [day2_dishes](items/day2_dishes.md)
- [x] [day2_leftovers](items/day2_leftovers.md)
- [x] [day3_dishes](items/day3_dishes.md)
- [x] [diary](items/diary.md)
- [x] [fingerprint_from_hook](items/fingerprint_from_hook.md)
- [x] [fingerprint_powder](items/fingerprint_powder.md)
- [x] [glass](items/glass.md)
- [x] [glass_with_tomatoes](items/glass_with_tomatoes.md)
- [x] [hairpin](items/hairpin.md)
- [x] [half_lemon](items/half_lemon.md)
- [x] [hammer](items/hammer.md)
- [x] [jalapeno_peppers](items/jalapeno_peppers.md)
- [x] [knife](items/knife.md)
- [x] [lemon](items/lemon.md)
- [x] [magnifying_glass](items/magnifying_glass.md)
- [x] [makeshift_fingerprint_kit](items/makeshift_fingerprint_kit.md)
- [x] [makeshift_virgin_mary](items/makeshift_virgin_mary.md)
- [x] [makeup_brush](items/makeup_brush.md)
- [x] [mashed_tomatoes](items/mashed_tomatoes.md)
- [x] [money](items/money.md)
- [x] [newspaper](items/newspaper.md)
- [x] [panties](items/panties.md)
- [x] [pants](items/pants.md)
- [x] [password_note](items/password_note.md)
- [x] [pepper](items/pepper.md)
- [x] [picture](items/picture.md)
- [x] [pin_note](items/pin_note.md)
- [x] [purse](items/purse.md)
- [x] [rebeca_fingerprints](items/rebeca_fingerprints.md)
- [x] [salt](items/salt.md)
- [x] [shirts](items/shirts.md)
- [x] [simcard](items/simcard.md)
- [x] [smartphone](items/smartphone.md)
- [x] [smartphone_incomplete](items/smartphone_incomplete.md)
- [x] [stepladder](items/stepladder.md)
- [x] [suitcase](items/suitcase.md)
- [x] [sweaters](items/sweaters.md)
- [x] [tape](items/tape.md)
- [x] [toiletries_bag](items/toiletries_bag.md)
- [x] [tomatoes](items/tomatoes.md)
- [x] [used_glass](items/used_glass.md)

## Closeups

#todo

- [ ] [bedroom_lamp](closeups/bedroom_lamp.md)
- [ ] [diary](closeups/diary.md)
- [ ] [gruesome_note](closeups/gruesome_note.md)
- [x] [newspaper](closeups/newspaper.md)
- [ ] [padlock](closeups/padlock.md)
- [ ] [purse](closeups/purse.md)
- [ ] [puzzle_fingerprints](closeups/puzzle_fingerprints.md)
- [ ] [smartphone](closeups/smartphone.md)
- [ ] [suitcase](closeups/suitcase.md)
- [ ] [wifi_router](closeups/wifi_router.md)

## Locations

#todo

- [x] [bedroom](locations/bedroom.md)
- [ ] [courthouse_room](locations/courthouse_room.md)
- [ ] [hall](locations/hall.md)
- [ ] [kitchen](locations/kitchen.md)
- [ ] [living_room](locations/living_room.md)
- [ ] [upper_hall](locations/upper_hall.md)