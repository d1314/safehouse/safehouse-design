# Intro sequence

#sfx

The introduction is based completely on a sound effect track (with descriptive subtitles for accessibility), which builds up the tension and atmosphere until a hard cut when the characters enter the safehouse. This is the transcript of that track:

We're in a car, but the sounds are muffled. We hear driving sounds, short rumbles, leather seats moving. Additionally, we hear a female person breathing.

There are two more persons in the car. One person is [rebeca](../characters/rebeca.md), the other one is an [unnamed officer](../characters/officer.md). After some seconds, they start to talk.

> [officer](../characters/officer.md)
> So. Officer Pliego in heat again?

> [rebeca](../characters/rebeca.md)
> Huh?

> [officer](../characters/officer.md)
> Yeah! One week of hard work ahead!

> [rebeca](../characters/rebeca.md)
> Shut up, Frank.

> [officer](../characters/officer.md)
> Woah, kitty's angry?

> [rebeca](../characters/rebeca.md)
> I said shut up.

> [officer](../characters/officer.md) *chuckles*

(seconds pass)

> [rebeca](../characters/rebeca.md)
> Around the corner. I'm calling him.

> [officer](../characters/officer.md)
> Who's with you?

> [rebeca](../characters/rebeca.md)
> Calvin.

> [officer](../characters/officer.md)
> Ah. At least someone with a pair of balls.

> [rebeca](../characters/rebeca.md)
> If you don't shut up, Frank, you'll loose yours in an instant for god's sake.

> [officer](../characters/officer.md)
> Okay! Okay! Calm down. I'm just messing with you.

> [rebeca](../characters/rebeca.md)
> Jesus.

*Dialing sounds*

> [rebeca](../characters/rebeca.md)
> Rebeca Pliego. Badge 31592. 315-92, yes.
> (beat)
> 13441
> Yes. We're approaching.
> (beat)
> Frank.
> (beat)
> Yeah.

> [officer](../characters/officer.md)
> (shouts) Hey Calvin!

> [rebeca](../characters/rebeca.md)
> Yeah. Bye.

*Mobile phone beep*

> [officer](../characters/officer.md)
> So what's up with cocoa back there?

> [rebeca](../characters/rebeca.md)
> Fuck, Frank! She can hear you.
> (turns around)
> I'm sorry, Marjorie. 