# Plop

#sfx

A sound which is something between a gunshot and a blown lightbulb.

It is played at the end of Marjorie's nightmare. In that nightmare, it represents a gunshot, but in reality it's just a blown lightbulb.