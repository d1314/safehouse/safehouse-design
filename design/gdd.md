# Safe House - Game Design Document

## Introduction

This is the game design document for "Safe House", an atmospheric point and click thriller.

It covers all the details about the game’s concept, design, assets and story.

## Short abstract

A witness to a mob murder is taken into a safe house by the police for five days before her testimony. Although she's protected by two police officers, the house might not be as safe as everybody thinks.

---

**WARNING** Spoilers start from here.

---

## Abstract

A witness to a mob murder is taken into a safe house by the police for five days before her court appearance. She's protected by two police officers. One of them is on the mob's payroll and previously staged the suicide of a witness 18 years ago. He first tries to scare the witness with the story of the suicide by leaving scary notes and intimidating her. On the fourth day, the facade is dropped, and he first tries to kill the other officer. The witness interrupts him, and then he tries to kill her, but she is able to hold him back and is saved in the last minute by the other officer, who was injured but is still alive.

## Characters

- [Marjorie](characters/marjorie.md#Marjorie), the witness
- [Calvin](characters/calvin.md#Calvin), a police office
- [Rebeca](characters/rebeca.md#Rebeca), a police officer
- [Brianna](characters/brianna.md#Brianna), the district attorney
- [Igino](characters/igino.md#Igino), the mob boss
- [Jacob](characters/jacob.md#Jacob), the former victim (deceased)

## Music

The idea for the music is, that the characters Marjorie, Calvin and Brianna have themes. Each of these themes have different variations and reprises that are used throughout the game to gently hint which character is currently in focus of the game.

The themes should reflect the character's personality or functions in the game:

- [Marjorie](music/marjorie.md#Marjorie)
- [Calvin](music/calvin.md#Calvin)
- [Brianna](music/brianna.md#Brianna)

## Location

The game takes place in an unknown, American-style suburb.

## Hint system

If the player doesn't take any relevant action anymore to solve the current puzzle, [marjorie](characters/marjorie.md) will speak one of the lines tagged with #💡 of the current puzzle.

The lines spoken depend on what actions the player has already taken or not taken.

The lines are spoken like thoughts.

## Game structure

The game is structured by chapters for each day. The chapters aren't visible to the player, though, and can only be distinguished by the change of scenery with morning/noon/evening/night scenes.

This is a short overview of the different scenes:

### Day 1

#### Morning

[marjorie](characters/marjorie.md) is escorted by [rebeca](characters/rebeca.md) to the safe house and are greeted by [calvin](characters/calvin.md) Calvin gives [marjorie](characters/marjorie.md) a tour of the house which ends in her room. [marjorie](characters/marjorie.md) unpacks. She has smuggled a phone into the safe house and calls her mother, but gets interrupted by [rebeca](characters/rebeca.md), who screams at her, grabs her phone and storms off.

#### Noon

They talk over dinner about what happened. [calvin](characters/calvin.md) tries to comfort [marjorie](characters/marjorie.md). [rebeca](characters/rebeca.md) is still irked at Marjorie.
After they finished eating, [marjorie](characters/marjorie.md) cleans the dishes. While she's at it, the window gets fogged and a noose and the words "You're next" appear. She startles and let's loose of the dishes, which crash on the floor. [rebeca](characters/rebeca.md) comes around to ask if everything's okay. When [marjorie](characters/marjorie.md) looks again at the window, the fog and the text is gone.

#### Afternoon

[brianna](characters/brianna.md) visits and reveals the [igino](characters/igino.md) back story.

#### Evening

[marjorie](characters/marjorie.md) goes up to her room to find a noose hanging from the ceiling. She screams, runs away and bumps into [rebeca](characters/rebeca.md) in the hall. While [rebeca](characters/rebeca.md) tries to explain, [calvin](characters/calvin.md) runs past them, into her room and quickly removes the noose (unknown to the player). [rebeca](characters/rebeca.md) follows him into the room.

[calvin](characters/calvin.md) and [rebeca](characters/rebeca.md) tell [marjorie](characters/marjorie.md) to come into the room and she sees that the noose is gone. [calvin](music/calvin.md) tells her, that because of the stress she must have been hallucinating. [rebeca](characters/rebeca.md) is strangely quiet and seemingly distracted.

### Day 2

#### Morning

[marjorie](characters/marjorie.md), [calvin](characters/calvin.md) and [rebeca](characters/rebeca.md) eat breakfast. [calvin](characters/calvin.md) reads The Standard. In the dialog, [marjorie](characters/marjorie.md) tries to gather more information about [igino](characters/igino.md) and La Famiglia.

After finishing the breakfast, [marjorie](characters/marjorie.md) goes into her room and finds a gruesome drawing with an eerie children's poem on her bed, which leads her to the combination of the [padlock](items/padlock.md) on the [bedroom_wardrobe_drawer_lower](items/bedroom_wardrobe_drawer_lower.md) in her room.

She opens the lock with the combination, and finds the fake diary of [jacob](characters/jacob.md) Grimes. Downstairs, she finds [rebeca](characters/rebeca.md) and talks to her. She reveals that [jacob](characters/jacob.md) Grimes was found dead 18 years ago. (It is not yet revealed, that [jacob](characters/jacob.md) hung himself or that [calvin](characters/calvin.md) was assigned to that case).

As [rebeca](characters/rebeca.md) doesn't tell her more, [marjorie](characters/marjorie.md) wants to investigate. She needs her phone for that.

[rebeca](characters/rebeca.md) sits on the couch, [marjorie](characters/marjorie.md)'s phone sticking out of one of her pockets. [marjorie](characters/marjorie.md) needs to walk into the kitchen and look through the hatch to get it in the right moment.

[marjorie](characters/marjorie.md)'s SIM card is gone, so she needs wifi. There actually is Wi-Fi in the house used by the detectives, but it's locked.

[marjorie](characters/marjorie.md) traces the Wi-Fi signal to a loose board in the upstairs hall. She gets a knife from the kitchen and pries open the board to find the router. She turns the router around to finds a password. It isn't the right one, though. A paper in the junk in the kitchen shows how to decipher it.

She logs into the Wi-Fi and searches for "[jacob](characters/jacob.md) Grimes" to find that he had hung himself in her room 18 years ago and that [calvin](characters/calvin.md) was assigned to that case.

#### Afternoon

[brianna](characters/brianna.md) visits and [marjorie](characters/marjorie.md) asks her about [jacob](characters/jacob.md)'s story.

#### Night

[marjorie](characters/marjorie.md) wakes up from a sound, sits up in her bed and sees [igino](characters/igino.md) in the doorway, aiming a gun at her. He shoots and she wakes up and quickly sits up, gasping. It's early morning on day 3.

### Day 3

#### Early morning

[marjorie](characters/marjorie.md) sits in her bed. Her room is dark, and only the dim light from the windows sheds some light into the room.

[marjorie](characters/marjorie.md) gets up to find the light switch, but the lamp can't be turned on since the bulb has blown.

She finds the lamp, but it's too high for her. She finds a stepladder in one cupboard of the kitchen and uses it to get to the lamp.

She unscrews the bulb in the lamp, and then goes into the lower hall where she finds a matching bulb, which is too hot to touch. She fetches a cleaning cloth from the kitchen, and unscrews it with that.

Upon screwing in the bulb into her lamp again, the light immediately switches on, and she sees that the lamp hangs on a sturdy hook. There's also something on the hook, but it's too tiny to see.

She finds a magnifying glass in a drawer in the kitchen.

She inspects the hook with the magnifying glass and finds hemp fibres. Slowly she realizes, that somebody is trying to scare her and that the noose actually *was* there on the evening of day 1.

She wants to investigate if somebody left fingerprints there. She suspects [rebeca](characters/rebeca.md).

#### Morning

[marjorie](characters/marjorie.md) and [calvin](characters/calvin.md) sit and eat breakfast. [rebeca](characters/rebeca.md) is at the now open cupboard in the living room. [calvin](characters/calvin.md) tells her to join them, to which she reluctantly agrees to. She leaves the cupboard open.

As they are eating, [marjorie](characters/marjorie.md) tries to grill [rebeca](characters/rebeca.md) to find out whether she is trying to scare her. At the same time, [rebeca](characters/rebeca.md) tries to test Calvin to find out more. They get into an argument. [calvin](characters/calvin.md) intervenes, and something he says triggers a reaction from [rebeca](characters/rebeca.md). Rebeca backs off and tells Calvin that she will go shopping, and leaves (the cupboard is still open).

After breakfast, [marjorie](characters/marjorie.md) takes the dishes once again to the kitchen.

[marjorie](characters/marjorie.md) finds a make-up brush in the trash.

She additionally gathers adhesive tape from a drawer in the hall, and the fingerprint powder from the unlocked cupboard.

As she's back in her room after getting all the things, [brianna](characters/brianna.md) arrives and she is interrupted.

#### Noon

[brianna](characters/brianna.md) visits and [marjorie](characters/marjorie.md) questions her about [rebeca](characters/rebeca.md).

#### Evening

In the meantime, [rebeca](characters/rebeca.md) is back. [marjorie](characters/marjorie.md) gets the fingerprints from the hook and tricks [rebeca](characters/rebeca.md) into drinking from a glass that [marjorie](characters/marjorie.md) snatches and gets her fingerprints off.

She compares the fingerprints, and they match. She's now convinced that [rebeca](characters/rebeca.md) is trying to scare her.

### Day 4

#### Early morning

[marjorie](characters/marjorie.md) wakes up to the sound of a fight. She walks down to find [calvin](characters/calvin.md) shooting [rebeca](characters/rebeca.md). [calvin](characters/calvin.md) turns around and comes at her. In fear, she turns around and runs into her room and pushes a cupboard in front of her door.

[calvin](characters/calvin.md) first tries to sweet-talk her, but ultimately looses his nerves and screams at her. He pushes against the door. The cupboard moves and the door opens. [calvin](characters/calvin.md) comes in, but is gunned down by the wounded [rebeca](characters/rebeca.md), who collapses.

### Day 5

#### Afternoon

[marjorie](characters/marjorie.md) sits with [brianna](characters/brianna.md). They talk about [calvin](characters/calvin.md). In the end, [marjorie](characters/marjorie.md) confirms to [brianna](characters/brianna.md) that she will appear in court and make her testimony.

The end.

## Scene description

### INTRODUCTION

*Cutscene starts*

Location: None (Black screen)

Music: None

Play [sfx/intro.md](sfx/intro.md)

Starting credits:

- deep entertainment presents
- VA Marjorie
- VA Calvin
- VA Rebeca
- VA Brianna
- Safehouse

After the sound effect ended: 

#💬 [INTRODUCTION_MORNING](shorts/introduction.ink)

*Fade in.*

### DAY 1 - INTRODUCTION - MORNING

Location: [Hall](locations/hall.md#Hall)

Music: None

Play [filling_form](sfx/filling_form.md)

[marjorie](characters/marjorie.md), [calvin](characters/calvin.md) and [rebeca](characters/rebeca.md) stand in the [hall](locations/hall.md), in front of the [front_door](items/front_door.md). We see a backpack, a bag and a [suitcase](items/suitcase.md) in front of them. The backpack belongs to [calvin](characters/calvin.md), the bag to [rebeca](characters/rebeca.md) and the [suitcase](items/suitcase.md) to [marjorie](characters/marjorie.md).

They stand there for a few seconds, looking around. [marjorie](characters/marjorie.md) is frightened, she is taking in the uncommon surroundings. [rebeca](characters/rebeca.md) is annoyed, and filling out a form.

[calvin](characters/calvin.md) calmly looks around.

(beat) Stop sound effect.

#💬 [DAY1_INTRODUCTION_MORNING](shorts/introduction.ink)

[rebeca](characters/rebeca.md) takes her bag and walks off to the [living_room](locations/living_room.md).

(beat)

[calvin](characters/calvin.md) looks at [marjorie](characters/marjorie.md), and nods in a friendly manner. [marjorie](characters/marjorie.md) tries to smile. [calvin](characters/calvin.md) takes his backpack and walks off to the [living_room](locations/living_room.md), too.

[marjorie](characters/marjorie.md) grabs her [suitcase](items/suitcase.md).

*Fade out*

### DAY 1 - MORNING

Location: [Bedroom](locations/bedroom.md#Bedroom)

Music: [Marjorie Relaxed](music/marjorie.md#Marjorie%20Relaxed)

*Fade in*

[marjorie](characters/marjorie.md) enters the [bedroom](locations/bedroom.md) and puts her [suitcase](items/suitcase.md) on the [bedroom_bed](items/bedroom_bed.md).

Play [suitcase_bed](sfx/suitcase_bed.md)

She looks around the room and lets out a sigh.

*Cutscene ends*

#### PUZZLE 1.1: Unpack

#💡 [HINTS_PUZZLE_1_1](hints.ink)

Click on the [suitcase](items/suitcase.md).

Click (unpack) the items in the [suitcase](closeups/suitcase.md).

When [marjorie](characters/marjorie.md) unpacks the pants, her [smartphone](items/smartphone.md) falls out and smashes into four pieces on the floor : (Play [object_crashing](sfx/object_crashing.md))

- [smartphone_incomplete](items/smartphone_incomplete.md)

- [battery](items/battery.md)

- [cover](items/cover.md)

- [simcard](items/simcard.md)

They slide in all directions.

#💬 [PUZZLE_1_1](shorts/day1.ink)

#### PUZZLE 1.2: Assembling the phone

#💡 [HINTS_PUZZLE_1_2](hints.ink)

Find the four items by examining the things in the [bedroom](locations/bedroom.md):

- [smartphone_incomplete](items/smartphone_incomplete.md) under [bedroom_bed](items/bedroom_bed.md)
- [simcard](items/simcard.md) under [bedroom_wardrobe](items/bedroom_wardrobe.md)
- [cover](items/cover.md) under [bedroom_nightstand](items/bedroom_nightstand.md)
- [battery](items/battery.md) under [bedroom_stool](items/bedroom_stool.md)

[PUZZLE_1_2_1](shorts/day1.ink)

Combine the items with the [smartphone_incomplete](items/smartphone_incomplete.md):

- [simcard](items/simcard.md)

- [battery](items/battery.md)

- [cover](items/cover.md)

#💬 [PUZZLE_1_2_2](shorts/day1.ink)

#### PUZZLE 1.3: Resetting the phone

#💡 [HINTS_PUZZLE_1_3](hints.ink)

Click the [suitcase](items/suitcase.md) again and continue unpacking. The last item is the [purse](items/purse.md).

Open the [purse](items/purse.md). Switch to [CLOSEUP_PURSE](closeups/purse.md)

Take the [hairpin](items/hairpin.md) and use it to reset the [smartphone](items/smartphone.md).

#💬 [PUZZLE_1_3](shorts/day1.ink)

#### PUZZLE 1.4: Finding the PIN

#💡[HINTS_PUZZLE_1_4](hints.ink)

Find the note in the [purse](items/purse.md). Enter the correct number in [PIN screen](closeups/smartphone.md#PIN%20screen)

### DAY 1 - CONTINUED - MORNING

*Cutscene starts*

Location: [Bedroom](locations/bedroom.md#Bedroom)

Music: [Marjorie Conflict](music/marjorie.md#Marjorie%20Conflict)

[marjorie](characters/marjorie.md) dials (Play [dialing](sfx/dialing.md)). After some moments, we hear:

#💬 [DAY1_CONTINUED_MORNING](shorts/day1.ink)

[rebeca](characters/rebeca.md) exits the room, slamming the door. (Play [door_slam](sfx/door_slam.md), Music stops hard)

*Blackout*

### DAY 1 - NOON

Location: [Living room](locations/living_room.md#Living%20room)

Music: [Calvin jazzy](music/calvin.md#Calvin%20jazzy)

Play [people_eating](sfx/people_eating.md)

*Fade in*

[marjorie](characters/marjorie.md), [rebeca](characters/rebeca.md) and [calvin](characters/calvin.md) are sitting on the [living_room_table](items/living_room_table.md), eating takeaway Chinese food. [day1_leftovers](items/day1_leftovers.md) and [day1_dishes](items/day1_dishes.md) are on the [living_room_table](items/living_room_table.md). The mood is tense.

#💬 [DAY1_NOON](shorts/day1.ink)

*Cutscene ends*

#### PUZZLE 1.5: Exploring

#💡 "I better clean up the table.", "Let's clean the dishes.", "Let's throw away the trash.", "Maybe I should apologize to Rebeca.", "Maybe I should talk to Calvin."

The player is free to roam the house. They need to do the following to continue:

- Take the [day1_leftovers](items/day1_leftovers.md) and [day1_dishes](items/day1_dishes.md) from the [living_room_table](items/living_room_table.md)

- Bring the [day1_leftovers](items/day1_leftovers.md) to the [kitchen_trash_can](items/kitchen_trash_can.md)

- Clean the [day1_dishes](items/day1_dishes.md) in the [kitchen_sink](items/kitchen_sink.md)

This triggers the following cutscene:

Cleaning the dishes heats up the kitchen and fogs the window. While [marjorie](characters/marjorie.md) is cleaning, the image of a noose and the words "You're next" become visible in the window. [marjorie](characters/marjorie.md) looks around, startles and drops the dishes which crash on the floor.

[rebeca](characters/rebeca.md) enters the kitchen. #todo 

- Talk to [calvin](characters/calvin.md): [calvin1](dialogs/calvin1.ink)

- Talk to [rebeca](characters/rebeca.md): [rebeca1](dialogs/rebeca1.ink)

Play [closing_front_door](sfx/closing_front_door.md).

> [brianna](characters/brianna.md)
> (shouting) Marjorie?

> [marjorie](characters/marjorie.md)
> (shouting) Coming!

*Fade out*


### DAY 1 - AFTERNOON

Location: [Living room](locations/living_room.md#Living%20room)

Music: [brianna](music/brianna.md)

*Fade in*

[marjorie](characters/marjorie.md) is sitting on the [living_room_sofa](items/living_room_sofa.md) with [brianna](characters/brianna.md). [brianna](characters/brianna.md)'s open suitcase lies on the couch table, [brianna](characters/brianna.md) opened a big folder that's now on her lap. [marjorie](characters/marjorie.md) is nervous.

[They talk.](dialogs/brianna1.ink)

*Fadeout*

### DAY 1 - EVENING

Location: [Living room](locations/living_room.md#Living%20room)

No music.

*Cutscene starts*

*Fade in*

[calvin](characters/calvin.md) and [rebeca](characters/rebeca.md) sit on opposite ends of the sofa with [calvin](characters/calvin.md) sitting closer to the hall.

[marjorie](characters/marjorie.md) is standing behind them.

> [marjorie](characters/marjorie.md)
>
> (yawns) All right, I'm off to bed. Have a good night.

> [calvin](characters/calvin.md)
>
> You too.

> [rebeca](characters/rebeca.md)
>
> Bye.

[marjorie](characters/marjorie.md) exits the [living_room](locations/living_room.md), goes through the [hall](locations/hall.md), up the stairs, through the [upper hall](locations/upper_hall.md) into the [bedroom](locations/bedroom.md) to find...

A noose dangling from the lamp.

Music: [Marjorie Horror](music/marjorie.md#Marjorie%20Horror)

[marjorie](characters/marjorie.md) screams, then runs out of the [bedroom](locations/bedroom.md), through the [upper hall](locations/upper_hall.md), down the stairs. In the [hall](locations/hall.md), she bumps into [rebeca](characters/rebeca.md).

> [rebeca](characters/rebeca.md)
>
> (concerned) What is it?

> [marjorie](characters/marjorie.md)
>
> (freaked out, shouting) There's a noose in my room! Oh my God!

> [rebeca](characters/rebeca.md)
> 
> A nose?

[calvin](characters/calvin.md) runs past the two up the stairs, draws his gun. 

> [marjorie](characters/marjorie.md)
>
> (shouting) Yes. By the lamp. Oh my *God*.

[rebeca](characters/rebeca.md) runs up the stairs.

Seconds pass. [marjorie](characters/marjorie.md) walks up and down the hall.

> [calvin](characters/calvin.md)
>
> (from above) Marjorie?

> [marjorie](characters/marjorie.md)
>
> Yes?

> [calvin](characters/calvin.md)
>
> Can you come up please?

[marjorie](characters/marjorie.md) climbs the stairs, passes the [upper hall](locations/upper_hall.md) to see [calvin](characters/calvin.md) and [rebeca](characters/rebeca.md) in the [bedroom](locations/bedroom.md), but no noose. [calvin](characters/calvin.md) is panting, but calm. [rebeca](characters/rebeca.md) is looking at [calvin](characters/calvin.md) with an irritated expression.

> [marjorie](characters/marjorie.md)
>
> What the...?

> [calvin](characters/calvin.md)
>
> (interrupts, calm) Marjorie? Is everything alright?

> [marjorie](characters/marjorie.md)
>
> (starts to cry, shouts) There was a noose! A fucking noose! I'm not crazy!

> [calvin](characters/calvin.md)
>
> (goes to her) It's okay, it's okay.

> [marjorie](characters/marjorie.md)
>
> (distressed) I'm not crazy. It was there.

[rebeca](characters/rebeca.md) snaps out of it, then exits the room. [calvin](characters/calvin.md) looks after her.

The screen slowly fades out over the next line and [marjorie](characters/marjorie.md) sobbing.

> [calvin](characters/calvin.md)
> 
> It's okay, Marjorie. Calm down.

[marjorie](characters/marjorie.md) sobs.

### DAY 2 - MORNING

*Cutscene ends*

Location: [living_room](locations/living_room.md)

Music: [Calvin jazzy](music/calvin.md#Calvin%20jazzy)

Play [people_eating](sfx/people_eating.md)

[marjorie](characters/marjorie.md), [calvin](characters/calvin.md) and [rebeca](characters/rebeca.md) are having breakfast at the [living_room_table](items/living_room_table.md). There's toast, scrambled eggs, bacon and jam. The bacon is next to [rebeca](characters/rebeca.md). [calvin](characters/calvin.md) is reading a copy of the newspaper "The Standard" and randomly sips from a cup of coffee, [rebeca](characters/rebeca.md) is eating muesli out of a bowl and staring out of the window. [marjorie](characters/marjorie.md) tries to make conversation.

A [dialog](dialogs/day2breakfast.ink) unfolds.

*Cutscene starts*

After it, we fade out, and fade in again. [rebeca](characters/rebeca.md) is now sitting on the [living_room_sofa](items/living_room_sofa.md) against the wall with the hatch, reading a file. She has a glass of water on the table in front of her. She leans a bit to the left while reading the file, but glances up from time to time to drink a sip from a glass before her.

[calvin](characters/calvin.md) has put the paper on the desk and reads. [marjorie](characters/marjorie.md) stands. [day2_dishes](items/day2_dishes.md) and the [day2_leftovers](items/day2_leftovers.md) are on the [living_room_table](items/living_room_table.md).

Music: [Marjorie Relaxed](music/marjorie.md#Marjorie%20Relaxed)

> [marjorie](characters/marjorie.md)
>
> Let me put myself to use and clean this up.

> [calvin](characters/calvin.md)
>
> Thank you, Marjorie.

*Cutscene ends*

The player can explore freely now.

(Optional) Take the [day2_dishes](items/day2_dishes.md) and the [day2_leftovers](items/day2_leftovers.md) from the table. Go into the [kitchen](locations/kitchen.md). Put the [day2_leftovers](items/day2_leftovers.md) into the [kitchen_trash_can](items/kitchen_trash_can.md), clean the [day2_dishes](items/day2_dishes.md) in the [kitchen_sink](items/kitchen_sink.md).

The player has to complete the following puzzles:

#### PUZZLE 2.1: The Diary

#💡 "I'm still tired.", "What's that on my bed?", "A locked drawer. Strange.", "I wonder what's in that locked drawer.", "I should ask the others about this strange diary."

Go up to [bedroom](locations/bedroom.md), find the [gruesome_note](items/gruesome_note.md).

Solve the note puzzle to get the combination.

Open the [padlock](items/padlock.md) at the [bedroom_wardrobe_drawer_lower](items/bedroom_wardrobe_drawer_lower.md) using the combination. Find the [diary](items/diary.md). Read the [diary](items/diary.md).

Go to the [living_room](locations/living_room.md). [calvin](characters/calvin.md) is gone. Talk to [rebeca](characters/rebeca.md): [rebeca2](dialogs/rebeca2.md)

> [marjorie](characters/marjorie.md)
> Jacob Grimes. Maybe I can find something about him online. (beat) If I'd only have my damned phone.

#### PUZZLE 2.2: Getting the phone back

#💡 "I need my phone back.", "How can I get my phone back?"

Go into the [kitchen](locations/kitchen.md) and look through the [kitchen_hatch](items/kitchen_hatch.md). The scene is switched to [living_room](locations/living_room.md) with [marjorie](characters/marjorie.md) looking through the [living_room_hatch](items/living_room_hatch.md). Wait for [rebeca](characters/rebeca.md) to lean to the left and read the file to snatch the [smartphone](items/smartphone.md) out of [rebeca](characters/rebeca.md)'s [pocket](items/pocket.md).

#### PUZZLE 2.3: Finding the wifi router

#💡 "Let's look Jacob up online.", "Maybe I can find the password for the Wifi somewhere.", "Don't they write the password of Wifis on these internet boxes?", "There needs to be one of this internet boxes somewhere!", "I wonder, what's under this floor board.", "The password doesn't work on the internet box. But why would they write it there then?", "Maybe that note from the trash can helps with the Wifi password."

Turn on the [smartphone](items/smartphone.md). Switch to [smartphone](closeups/smartphone.md), it shows no mobile connection, but an accessible wifi. Connecting to it requires a password though.

The wifi signal display is set to 2 of 4 bars in the [kitchen](locations/kitchen.md) and the [living_room](locations/living_room.md). It shows 3 of 4 bars in the [hall](locations/hall.md) and the [bedroom](locations/bedroom.md). It shows 4 of 4 bars in the [upper_hall](locations/upper_hall.md).

Looking at the [smartphone](items/smartphone.md) and selecting the wifi signal in the  [upper_hall](locations/upper_hall.md)  reveals a [upper_hall_loose_board](items/upper_hall_loose_board.md). It can be pried loose using a [knife](items/knife.md) from the [kitchen](locations/kitchen.md). Under the [upper_hall_loose_board](items/upper_hall_loose_board.md) the [upper_hall_wifi_router](items/upper_hall_wifi_router.md) is revealed. Using it switches to [wifi router](closeups/wifi_router.md) When turning it around, a wifi password is revealed.

Entering the password as-is on the [smartphone](closeups/smartphone.md) doesn't work.

In the [kitchen_trash_can](items/kitchen_trash_can.md) you find the [password_note](items/password_note.md), which, mentally combined with the password under the [upper_hall_wifi_router](items/upper_hall_wifi_router.md),  reveals the actual wifi password. Enter it into the [smartphone](items/smartphone.md) to connect to the wifi.

> [marjorie](characters/marjorie.md)
>
> There. Now, let's see what happened to Jacob Grimes. (pause)
>
> (to herself) Grimes, Grimes, Gri... Jacob Grimes. There you are. A "The Standard" article from eighteen years ago.
>
> (reading) "Jacob Grimes, witness in the La Famiglia trial on the murder of Derek Cole, was found dead this Thursday afternoon while in (stops) witness protection of the Police."
>
> (puts down the [smartphone](items/smartphone.md) briefly, then continues to read)
>
> "His body was found in his bedroom, where he apparently had hung himself. 'We can only assume, that the pressure and stress of the events affected him more than we realized. I deeply regret that we couldn't foresee this and help Jacob more', says (stops) Detective Calvin Andrews."

[marjorie](characters/marjorie.md)  puts down the [smartphone](items/smartphone.md). Blackout.

### DAY 2 - AFTERNOON

Location: [Living room](locations/living_room.md#Living%20room)

Music: [brianna](music/brianna.md)

[marjorie](characters/marjorie.md) sits on the sofa with [brianna](characters/brianna.md). [brianna](characters/brianna.md)'s open suitcase lies on the couch table, [brianna](characters/brianna.md) opened a big folder on her lap. [marjorie](characters/marjorie.md) is scared.

[They talk.](dialogs/brianna2.md)

*Fade out*

### DAY 2 - NIGHT

*Cutscene starts*

Location: [Bedroom](locations/bedroom.md#Bedroom)

No music.

*No ambient*

*Directly show the scene*

It's night. We see the [bedroom](locations/bedroom.md) only lit from the windows to the left. We see part of the bed with very hard shadows. [marjorie](characters/marjorie.md) lies in the bed. After a few seconds, Marjorie wakes up and rises in bed.

Play [creaking_bed](sfx/creaking_bed.md)

Suddenly, the bedroom is lit up, we see [igino](characters/igino.md) standing in the door, slowly rising a gun. [marjorie](characters/marjorie.md) opens her mouth as if to scream, but no scream is heard.

Music: [Marjorie Panic](music/marjorie.md#Marjorie%20Panic)

### DAY 3 - EARLY MORNING

Location: [Bedroom](locations/bedroom.md#Bedroom)

We switch back to the dark [bedroom](locations/bedroom.md) and barely see the small explosion of the lightbulb, which makes a [plop](sfx/plop.md), which makes it blur to the gun scene.

No music.

[marjorie](characters/marjorie.md) startles up. Then realizes that she's safe. [marjorie](characters/marjorie.md) rises. She tries to turn on the [bedroom_lamp](items/bedroom_lamp.md) using the [bedroom_light_switch](items/bedroom_light_switch.md) (Play [broken_light_switch](sfx/broken_light_switch.md)), which doesn't work.

> [marjorie](characters/marjorie.md)
>
> Hm. I guess the bulb's dead.

*Cutscene ends*

Music: [Marjorie Night](music/marjorie.md#Marjorie%20Night)

#### PUZZLE 3.1: Fixing the lamp

#💡 "I need to replace the light bulb. I need some light in here.", "I'm too small to reach the lamp.", "Now where's a lightbulb that fits into the socket?", "The lightbulb should fit."

Go into the [kitchen](locations/kitchen.md), get the [stepladder](items/stepladder.md) from the [kitchen_left_cupboard](items/kitchen_left_cupboard.md). Use the [stepladder](items/stepladder.md) with the [bedroom_lamp](items/bedroom_lamp.md). [marjorie](characters/marjorie.md) walks up the [stepladder](items/stepladder.md) and unscrews the [broken_bulb](items/broken_bulb.md).

Look at the lamps in the house. Only the bulb from the [hall_lamp](items/hall_lamp.md) matches. But it's too hot to touch. Find a [cloth](items/cloth.md) in the [kitchen](locations/kitchen.md) and use it to unscrew the [bulb](items/bulb.md).

Screw the [bulb](items/bulb.md) into the [bedroom_lamp](items/bedroom_lamp.md)

*Cutscene starts*

Music: [Marjorie Scary Night](music/marjorie.md#Marjorie%20Scary%20Night)

The light flashes on and [marjorie](characters/marjorie.md) gasps, startles, leans back and falls down the [stepladder](items/stepladder.md) onto the floor.

> [marjorie](characters/marjorie.md)
>
> What the?

She rises again, climbs the [stepladder](items/stepladder.md) again and looks up. The screen switches to [bedroom lamp](closeups/bedroom_lamp.md): We see that the lamps hangs on a sturdy metallic hook.

Music: [Marjorie Horror Strings](music/marjorie.md#Marjorie%20Horror%20Strings)

> [marjorie](characters/marjorie.md)
>
> That's... that's the hook the noose was hanging from. Are you fucking kidding me?
>
> (pause)
>
> What's that on the hook?

Back to Music: [Marjorie Night](music/marjorie.md#Marjorie%20Night)

*Cutscene ends*

#### PUZZLE 3.2 - Inspecting the hook

#💡 "What's that on the hook?", "There's this thing on the hook, but it's too small to see."

Get the [magnifying_glass](items/magnifying_glass.md) from the  [kitchen_right_drawer](items/kitchen_right_drawer.md).

Use the [magnifying_glass](items/magnifying_glass.md) with the [tiny_thing](items/tiny_thing.md) on the hook.

*Cutscene starts*

Music: [Marjorie Gaining Confidence](music/marjorie.md#Marjorie%20Gaining%20Confidence)

> [marjorie](characters/marjorie.md)
>
> What. The. Fuck? That's hemp. So there *was* an actual noose hanging here?

[marjorie](characters/marjorie.md) climbs down the ladder and goes up and down the room.

> [marjorie](characters/marjorie.md)
>
> I can't believe it. That's... (back and forth)
>
> but... but why? (back and forth)
>
> Is it about my testimony? (back and forth)
>
> I can't fucking believe it. (stops in the middle of the room)
>
> Does she want to scare me? Is she involved in this? (goes to the light switch)
>
> Nuh-uh, bitch. You can't scare me. Not. (hand on the light switch)
>
> Me.

Play [light_switch](sfx/light_switch.md)

*Blackout*

### DAY 3 - MORNING

Location: [Living room](locations/living_room.md#Living%20room)

Music: [Marjorie Relaxed](music/marjorie.md#Marjorie%20Relaxed)

[marjorie](characters/marjorie.md) and [calvin](characters/calvin.md) sit and eat breakfast. [rebeca](characters/rebeca.md) is at the now open [living_room_cupboard](items/living_room_cupboard.md) making it [living_room_cupboard_unlocked](items/living_room_cupboard_unlocked.md), fiddling around with its contents.

> [calvin](characters/calvin.md)
>
> Rebeca? Are you coming?

No answer.

> [calvin](characters/calvin.md)
>
> Rebeca?

> [rebeca](characters/rebeca.md) (grunts)
>
> *Fine*.

She comes over without locking the cupboard, drops down at the table and starts munching Muesli again.

A [dialog](dialogs/day3breakfast.md) unfolds.

> [calvin](characters/calvin.md)
>
> Ladies, please!

> [rebeca](characters/rebeca.md)
>
> (shouting) I would if you would try to grow up, missy!

> [calvin](characters/calvin.md)
>
> (louder) *Ladies!*

> [marjorie](characters/marjorie.md)
>
> (shouting) Oh, I'd grow up if you would be more empathic than a block of...

> [calvin](characters/calvin.md)
>
> (shouting) Ladies! Can we please calm down again?

> [rebeca](characters/rebeca.md)
>
> (shouting) Tell that to her!

> [calvin](characters/calvin.md)
>
> (shouting) I'm telling that to each of you. Stop that! I don't want to lock you up in your room!

Quiet. [rebeca](characters/rebeca.md) stares at [calvin](characters/calvin.md). Seconds pass.

> [rebeca](characters/rebeca.md)
>
> (snaps out of it, looks around) We need groceries.

[rebeca](characters/rebeca.md) storms off. Seconds pass.

> [calvin](characters/calvin.md)
>
> I'm... I'm sorry.

> [marjorie](characters/marjorie.md)
>
> No, please. I'm sorry. This was... I was...

> [calvin](characters/calvin.md)
>
> It's okay, Marjorie. It's really hard, I know.

Seconds pass. [calvin](characters/calvin.md) starts to clean the table.

> [marjorie](characters/marjorie.md)
>
> Ah, you know what? Why don't you sit down, and I take care of the dishes again? As an apology.

> [calvin](characters/calvin.md)
>
> You don't need to apologize, you know?

> [marjorie](characters/marjorie.md)
>
> Thank you, but still... I just need something to do.

> [calvin](characters/calvin.md)
>
> Okay then. Thank you.

He sits down again. She takes the [day3_dishes](items/day3_dishes.md) and goes to the [kitchen](locations/kitchen.md). As she wants to empty the dishes into the [kitchen_trash_can](items/kitchen_trash_can.md) (Play [trashcan_opens](sfx/trashcan_opens.md)), she stops.

Then she takes the [makeup_brush](items/makeup_brush.md) from the [kitchen_trash_can](items/kitchen_trash_can.md)

> [marjorie](characters/marjorie.md)
> I wonder... maybe that bitch left her fingerprints on the hook. Then I'd have proof.

She empties the dishes and closes the trashcan. Play [trashcan_closes](sfx/trashcan_closes.md)

Add [makeup_brush](items/makeup_brush.md).

*Cutscene ends*

(optional) Clean the [day3_dishes](items/day3_dishes.md) in the [kitchen_sink](items/kitchen_sink.md).

#### PUZZLE 3.3: Getting the fingerprint kit

#💡 "How do they take fingerprints in the movies again?", "I need some powder.", "How do I apply the powder onto the prints without messing them up?", "I need something to lift off the fingerprints.", "Time to get the fingerprints!"

Take the [makeup_brush](items/makeup_brush.md) from the [kitchen_trash_can](items/kitchen_trash_can.md). Take the [tape](items/tape.md) from [hall_sideboard](items/hall_sideboard.md). Get the [fingerprint_powder](items/fingerprint_powder.md) from the [living_room_cupboard_unlocked](items/living_room_cupboard_unlocked.md).

Combine them all into [makeshift_fingerprint_kit](items/makeshift_fingerprint_kit.md).

Go to the [bedroom](locations/bedroom.md). Look at the [bedroom_lamp](closeups/bedroom_lamp.md) and use the [makeshift_fingerprint_kit](items/makeshift_fingerprint_kit.md) on the hook.

*Cutscene starts*

Play [closing_front_door](sfx/closing_front_door.md)

> [brianna](characters/brianna.md)
>
> (shouting up) Marjorie? I'm here.

We exit the [bedroom_lamp](closeups/bedroom_lamp.md).

Marjorie takes a look at the lamp, sighs.

> [marjorie](characters/marjorie.md)
>
> *Coming!*

### DAY 3 - NOON

Location: [Living room](locations/living_room.md#Living%20room)

Music: [brianna](music/brianna.md)

[marjorie](characters/marjorie.md) sits on the sofa with [brianna](characters/brianna.md). [brianna](characters/brianna.md)'s open suitcase lies on the couch table, [brianna](characters/brianna.md) opened a big folder on her lap. [marjorie](characters/marjorie.md) is angry.

[They talk](dialogs/brianna3.md).

*Fadeout*

### DAY 3 - EVENING

Location: [Living room](locations/living_room.md#Living%20room)

Music: [Marjorie Relaxed](music/marjorie.md#Marjorie%20Relaxed)

Marjorie sits on the [living_room_sofa](items/living_room_sofa.md) as [rebeca](characters/rebeca.md) enters the [living_room](locations/living_room.md), she seems confused.

> [marjorie](characters/marjorie.md)
>
> (suspicious) Got everything you wanted? (referring to getting groceries)

[rebeca](characters/rebeca.md) subtly startles and looks at [marjorie](characters/marjorie.md)

> [rebeca](characters/rebeca.md)
>
> Huh?

> [marjorie](characters/marjorie.md)
>
> The groceries?

> [rebeca](characters/rebeca.md)
>
> (still confused) Gro... (realizes) oh yeah. Yeah.

[rebeca](characters/rebeca.md) exits the [living room](locations/living_room.md) and enters the [kitchen](locations/kitchen.md).

Marjorie stands up.

#### PUZZLE 3.4: Lifting fingerprints

#💡 "Finally I can take the fingerprints."

Go to the [bedroom](locations/bedroom.md).

Use the [makeshift_fingerprint_kit](items/makeshift_fingerprint_kit.md) with the [bedroom_hook](items/bedroom_hook.md) to get [fingerprint_from_hook](items/fingerprint_from_hook.md).

#### PUZZLE 3.5: Getting fingerprints from Rebeca

#💡 "I need to get the prints from Rebeca.", "How can I get prints from Rebeca?", "Maybe she'd like a drink.", "Where do I get a virgin mary in this hellhole?", "I still need things for the virgin mary.", "I think I got everything for the virgin mary now. Let's mix it.", 

Go to the [kitchen](locations/kitchen.md). [rebeca](characters/rebeca.md) stands to the right, looking out the window.

[Talk to Rebeca](dialogs/rebeca3.md)

Use the [smartphone](items/smartphone.md) to search for a recipe for a virgin mary.

Get the following items:

- [glass](items/glass.md) from a cupboard in the [kitchen](locations/kitchen.md)
- [knife](items/knife.md) from the drawer in the [kitchen](locations/kitchen.md)
- [lemon](items/lemon.md) from a bowl in the [living room](locations/living_room.md)
- [tomatoes](items/tomatoes.md) and [jalapeno_peppers](items/jalapeno_peppers.md) from the [kitchen_fridge](items/kitchen_fridge.md) in the [kitchen](locations/kitchen.md)
- [hammer](items/hammer.md) from the [hall_drawer](items/hall_drawer.md) in the [hall](locations/hall.md)
- [salt](items/salt.md) and [pepper](items/pepper.md) from the table in the [living room](locations/living_room.md)

Combine:

- [hammer](items/hammer.md) with [tomatoes](items/tomatoes.md) to get [mashed_tomatoes](items/mashed_tomatoes.md)
- [mashed_tomatoes](items/mashed_tomatoes.md) with [glass](items/glass.md)
- [lemon](items/lemon.md) with [knife](items/knife.md) to get [half_lemon](items/half_lemon.md)
- [half_lemon](items/half_lemon.md) with [glass_with_tomatoes](items/glass_with_tomatoes.md)
- [salt](items/salt.md) with [glass_with_tomatoes](items/glass_with_tomatoes.md)
- [pepper](items/pepper.md) with [glass_with_tomatoes](items/glass_with_tomatoes.md)
- [knife](items/knife.md) with [jalapeno_peppers](items/jalapeno_peppers.md) to get [cut_peppers](items/cut_peppers.md)
- [cut_peppers](items/cut_peppers.md) with [glass_with_tomatoes](items/glass_with_tomatoes.md) to get [makeshift_virgin_mary](items/makeshift_virgin_mary.md)

Give [makeshift_virgin_mary](items/makeshift_virgin_mary.md) to [rebeca](characters/rebeca.md).

> [rebeca](characters/rebeca.md)
>
> Huh? What? Is that...

> [marjorie](characters/marjorie.md)
>
> A makeshift Virgin Mary. As a peace offer.

> [rebeca](characters/rebeca.md)
>
> That's... (pause) That's genuinely nice. Thank you.

> [marjorie](characters/marjorie.md)
>
> You're welcome.

[rebeca](characters/rebeca.md) drinks.

> [rebeca](characters/rebeca.md)
>
> Not as good as in my favorite club, but still very good. Thanks, Marjorie.

> [marjorie](characters/marjorie.md)
>
> Here, let me take that.

Takes the [used_glass](items/used_glass.md) from [rebeca](characters/rebeca.md).

#### PUZZLE 3.6: Comparing fingerprints

#💡 "Okay, bitch! Time to get your fingerprints.", "I wonder, if the fingerprints match."

- Use the [makeshift_fingerprint_kit](items/makeshift_fingerprint_kit.md) with [used_glass](items/used_glass.md) to get [rebeca_fingerprints](items/rebeca_fingerprints.md)
- Use [rebeka_fingerprints](items/rebeca_fingerprints.md) with [fingerprint_from_hook](items/fingerprint_from_hook.md)

Marjorie exits the [kitchen](locations/kitchen.md), enters the [living_room](locations/living_room.md) and goes to the [living_room_table](items/living_room_table.md).

Switch to [closeups/puzzle_fingerprints](closeups/puzzle_fingerprints.md)

*Cutscene starts*

> [marjorie](characters/marjorie.md)
>
> They... they match.
>
> (pause)
>
> You. Bitch.

*Fade out*

### DAY 4 - EARLY MORNING

Location: [Bedroom](locations/bedroom.md#Bedroom)

No music.

We fade in to the night view of the [bedroom](locations/bedroom.md) and see [marjorie](characters/marjorie.md) sleeping.

After some seconds, play [glass_breaking](sfx/glass_breaking.md) and [marjorie](characters/marjorie.md) startles up.

> [marjorie](characters/marjorie.md)
>
> (to herself) What the?

We hear [muffled_shouts](sfx/muffled_shouts.md) from downstairs.

[marjorie](characters/marjorie.md) gets up.

*Cutscene ends*

Go down into the [living_room](locations/living_room.md) (the [muffled_shouts](sfx/muffled_shouts.md) change in volume as the player approaches) to see light in the kitchen and the door opened. Here, we can clearly hear the shouting:

Music: [Calvin revealing](music/calvin.md#Calvin%20revealing)

*Cutscene starts*

> [rebeca](characters/rebeca.md)
>
> (shouting) What the fuck, Calvin? You're a cop! Doesn't that mean anything to you?

[marjorie](characters/marjorie.md) ducks down and slowly approaches the kitchen.

> [calvin](characters/calvin.md)
>
> (shouting) The fuck it means to me! Did you have a look at your paycheck recently, Officer Pliego?

> [rebeca](characters/rebeca.md)
>
> (shouting) And that would make you switch sides? Money? Come on, Calvin! That's so cliché!

[marjorie](characters/marjorie.md) reaches the kitchen and we switch to the [kitchen](locations/kitchen.md) with her looking in. [rebeca](characters/rebeca.md) has backed up to the right side of the screen. [calvin](characters/calvin.md) is in the middle of the room. They don't notice Marjorie.

> [calvin](characters/calvin.md)
>
> (shouting) Of course it's money! It's always the money. A good conscience doesn't pay any bills!

> [rebeca](characters/rebeca.md)
>
> (shouting) Didn't you think somebody would find out eventually?

> [calvin](characters/calvin.md)
>
> (suddenly calm, diabolic) Oh, I made sure nobody would find out. At least nobody alive...

[calvin](characters/calvin.md) raises a gun at [rebeca](characters/rebeca.md). [marjorie](characters/marjorie.md) screams and runs out of the kitchen. We quickly switch back to the [living_room](locations/living_room.md). A [gunshot](sfx/gunshot.md) is heard and we see a short flicker from the kitchen door.

Music: [Calvin chasing](music/calvin.md#Calvin%20chasing)

Play [gunshot](sfx/gunshot.md)

*Cutscene ends*

#### PUZZLE 4.1: Survive!

#💡 "**Fuck! He's after me!**", "**Run!**"

From this on we need to hurry. Staying in the [living_room](locations/living_room.md), the [hall](locations/hall.md), the [upper hall](locations/upper_hall.md) or the (unboarded) [bedroom](locations/bedroom.md) more than 5 seconds will make [calvin](characters/calvin.md) appear and shoot (Play [gunshot](sfx/gunshot.md)). If that happens, we're taken back to the beginning of this puzzle.

Go through the [hall](locations/hall.md), the [upper hall](locations/upper_hall.md) into the [bedroom](locations/bedroom.md). Close the [bedroom_door](items/bedroom_door.md) and use the [bedroom_wardrobe](items/bedroom_wardrobe.md) to push it in front of the door.

*Cutscene starts*

Music: [Calvin switching](music/calvin.md#Calvin%20switching)

[marjorie](characters/marjorie.md) goes to the left of her room, trips and falls. Then we hear [calvin](characters/calvin.md) banging on the [bedroom_door](items/bedroom_door.md).

> [calvin](characters/calvin.md)
>
> (shouting) Marjorie! Open up!

Seconds pass, [calvin](characters/calvin.md) banging on the [bedroom_door](items/bedroom_door.md).

Play [door_bang](sfx/door_bang.md)

> [calvin](characters/calvin.md)
>
> (shouting) Open up! This is the police! (laughs)
>
> (falling back to his "nice" voice) Listen, Marjorie! I'm really sorry, I really am! You just happened to be at the wrong place at the wrong time, I'm afraid.

[calvin](characters/calvin.md) banging on the [bedroom_door](items/bedroom_door.md).

Play [door_bang](sfx/door_bang.md)

> [calvin](characters/calvin.md)
>
> (shouting) Let's face it, Mister Varone won't accept your blabbermouth in court!

Now [showdown](dialogs/showdown.md) is played. After all questions are answered, the [bedroom_wardrobe](items/bedroom_wardrobe.md) gives in, the [bedroom_door](items/bedroom_door.md) swings open and [calvin](characters/calvin.md) enters.

> [calvin](characters/calvin.md)
>
> It's the same with you. Why didn't you just chicken out like they usually do?

> [marjorie](characters/marjorie.md)
>
> (shouting) Usually?
>
> (quiet) Usually do? How many times have you pulled these tricks?

> [calvin](characters/calvin.md)
>
> I've been in the force longer than you've been alive, missy.

[calvin](characters/calvin.md) steps a bit closer

> [calvin](characters/calvin.md)
>
> Igino Varone is a... busy man.

[calvin](characters/calvin.md) takes out his gun.

> [marjorie](characters/marjorie.md)
>
> (cries out) NO!

> [calvin](characters/calvin.md)
>
> You should have just been so petrified that you'd retract your testimony. But you're too stubborn for your own good.

[calvin](characters/calvin.md) points his gun at [marjorie](characters/marjorie.md).

> [marjorie](characters/marjorie.md)
>
> (cries) nooo.

> [calvin](characters/calvin.md)
>
> So this is on you, Marjorie, I'm afraid. And poor Rebeca killed you. Ah well, what a mess.
>
> Goodbye, Marjorie.

> [marjorie](characters/marjorie.md)
>
> NO!

The screen flashes white, a [gunshot](sfx/gunshot.md) is heard.

*Music stops*

We see the [bedroom](locations/bedroom.md) again. [calvin](characters/calvin.md) still stands in the middle of the room, with the raised gun. [marjorie](characters/marjorie.md) is on the left.

Suddenly [calvin](characters/calvin.md) lowers his arm, we see blood on his shirt, he goes to his knees and then breaks down. (Play [calvin_breaking_down](sfx/calvin_breaking_down.md)])

(beat)

A wounded [rebeca](characters/rebeca.md) limps into the room, looks at [calvin](characters/calvin.md)'s body, then breaks down as well. (Play [rebeca_breaking_down](sfx/rebeca_breaking_down.md))

(beat)

Blackout.

### DAY 5 - NOON

Location: Unknown (black screen)

Music: None

> [marjorie](characters/marjorie.md)
>
> (spoken as in the intro) So this is it?

(beat)

> [brianna](characters/brianna.md)
>
> This is it.

### DAY 5 - NOON CONTINUED

Location: [Courthouse room](locations/courthouse_room.md#Courthouse%20room)

Music: None

We see the [courthouse room](locations/courthouse_room.md). [marjorie](characters/marjorie.md) sits on one end of the table, [brianna](characters/brianna.md) beside her.

> [marjorie](characters/marjorie.md)
>
> (exhales)

> [brianna](characters/brianna.md)
>
> You okay?

> [marjorie](characters/marjorie.md)
>
> Yes, I guess.

> [brianna](characters/brianna.md)
>
> I'm so sorry about what happen...

> [marjorie](characters/marjorie.md)
>
> (interrputs) It's okay, Brianna.

> [brianna](characters/brianna.md)
>
> I just don't understand how Calvin could do that all those times. This is a disaster.

> [marjorie](characters/marjorie.md)
>
> No doubt.

> [brianna](characters/brianna.md)
>
> And we have to revisit all the old cases. Each and every one where the witness bailed out in the last second. And that's a lot.

> [marjorie](characters/marjorie.md)
>
> All because of that fuck.

Silence.

> [marjorie](characters/marjorie.md)
>
> Oh, how's Rebeca?

> [brianna](characters/brianna.md)
>
> She is... recovering. She sends her regards.

> [marjorie](characters/marjorie.md)
>
> (surprised) She does?

> [brianna](characters/brianna.md)
>
> Yeah. Specifically, she said "Kick this fucker's ass!"

> [marjorie](characters/marjorie.md)
>
> (laughs) Yeah, that sounds more like her.

They laugh.

> [marjorie](characters/marjorie.md)
>
> She saved my life.

> [brianna](characters/brianna.md)
>
> I guess so. She said, on that evening with the noose, Calvin came rushing out of your bedroom with something tacked in his suit, but when he came back it was gone.

> [brianna](characters/brianna.md)
> And then Calvin said something about locking you up.

> [marjorie](characters/marjorie.md)
>
> (nodding) When we were fighting. I remember.

> [brianna](characters/brianna.md)
>
> Yes, and it came to Rebeca's mind, that Jacob was locked up as well and then she really got suspicious and started to investigate.

> [marjorie](characters/marjorie.md)
>
> (to herself) The groceries!

> [brianna](characters/brianna.md)
>
> What?

> [marjorie](characters/marjorie.md)
>
> Rebeca said she was about to get groceries but the cupboards were full. She was investigating then.

> [brianna](characters/brianna.md)
>
> Yeah and the next morning they got into a... fight. But you know the details.

> [marjorie](characters/marjorie.md)
>
> Yes. (pause)
>
> I need to apologize to Rebeca when she's out of the hospital. I suspected that the one trying to scare me was her. I even found her fingerprints on the hook in my room.

> [brianna](characters/brianna.md)
>
> I guess she did some investigating of her own there.

> [marjorie](characters/marjorie.md)
>
> I guess so.

Pause.

> [brianna](characters/brianna.md)
>
> You're ready?

> [marjorie](characters/marjorie.md)
>
> Yeah.
>
> Let's... (smiling) Let's kick ass.

They rise.

Fadeout.

Music: [Marjorie Triumph](music/marjorie.md#Marjorie%20Triumph)

The end. Roll the credits.

## Credits

### Written by

Dennis Ploeger

### Creative Consultants

Mikael Nyqvist
Jon Ingold
Rose Newell
Tracey Mitchell

### Sound Effects

Sounds used from freesound.org under the Creative Commons License

[Aesterial Arts](https://freesound.org/people/Aesterial-Arts/)
