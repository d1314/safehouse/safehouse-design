This is the Safehouse dialog index.

// Shorts
INCLUDE shorts/introduction.ink
INCLUDE shorts/day1.ink

// Hints
INCLUDE hints.ink

// Dialogs
INCLUDE dialogs/brianna1.ink
INCLUDE dialogs/calvin1.ink
INCLUDE dialogs/day2breakfast.ink
INCLUDE dialogs/rebeca1.ink