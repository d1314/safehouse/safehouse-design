# Hall

#ambient

Ambient played in the [hall](../locations/hall.md) and - in a lower volume - in [upper_hall](../locations/upper_hall.md).

It's composed of static with wind from the outside, and distant cars passing by.

# Hall Night

Like [Hall](#Hall), but without the cars.