# Living room

#ambient 

An ambient mixed from static and wind on the outside. Sometimes a chair creaks or the sofa rustles, since people are using them.

# Living room night

Like [Living room](living_room.md#Living%20room) but without the people.