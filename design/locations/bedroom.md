# Bedroom

#bedroom #location 

## Concept art

(Camera position "Bedroom")

![](../images/concept_bedroom.png)

## Description

The bedroom is Marjorie's retreat in the Safehouse. We see a [bedroom_bed](../items/bedroom_bed.md), a [bedroom_nightstand](../items/bedroom_nightstand.md), a [bedroom_wardrobe](../items/bedroom_wardrobe.md) and a [bedroom_stool](../items/bedroom_stool.md). It's quite scarcely furnished.

There's a violet carpet on the floor.

The room has one [bedroom_door](../items/bedroom_door.md) in the middle of the back wall. To the right of it is the [bedroom_wardrobe](../items/bedroom_wardrobe.md), which is later used to block the [bedroom_door](../items/bedroom_door.md). The [bedroom_door](../items/bedroom_door.md) opens to the inside of the room.

To the left is a window. The shadow casting through it shows that there's a tree on the outside, swaying in the wind.

There's a [bedroom_lamp](../items/bedroom_lamp.md) on the ceiling in the middle of the room.

We need a night and a day variant.

## SFX

Walking plays [footsteps_carpet](../sfx/footsteps_carpet.md)

## Ambient

- [bedroom](../ambients/bedroom.md)