# Hall

#hall #location

## Concept art

(Camera position "Hall")

![](../images/concept_hall.png)

## Descriptions

The hall is the very first room the player sees. It is shown directly after the intro.

After that, the hall is usually just a walk-through room.

There's the [front_door](../items/front_door.md) and the [hall_sideboard](../items/hall_sideboard.md) here. Wooden Stairs are leading up to the [upper_hall](upper_hall.md). Clicking on the front of the image leads to the [living_room](living_room.md).

A [hall_lamp](../items/hall_lamp.md) illuminates the room in the night scenes. It can be turned on and off via [hall_lightswitch](../items/hall_lightswitch.md).

The floor is made of wood tiles.

We need a night and day variant, plus variants with the lamp turned off in both.

## SFX

Walking on the hall play [footsteps wood](../sfx/footsteps_wood.md)

## Ambient

- Day: [Hall](../ambients/hall.md#Hall)
- Night: [Hall Night](../ambients/hall.md#Hall%20Night)