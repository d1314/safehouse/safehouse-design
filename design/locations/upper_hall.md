# Upper hall

#upperhall #location 

## Concept art

![](../images/concept_upper_hall.png)

## Description

The upper hall is a windowless room connecting the [bedroom](bedroom.md) and the [hall](hall.md) downstairs. There's also an [other_door](../items/other_door.md) leading to the (inaccessible) room, where [calvin](../characters/calvin.md) and [rebeca](../characters/rebeca.md) stay.

There's [upper_hall_lamp](../items/upper_hall_lamp.md) on the wall.

The floor is made of wood boards with one [upper_hall_loose_board](../items/upper_hall_loose_board.md), which hides the [upper_hall_wifi_router](../items/upper_hall_wifi_router.md).

We need a night and a day variant.

## SFX

Walking plays [footsteps_wood](../sfx/footsteps_wood.md)

## Ambient

 [upper_hall](../ambients/upper_hall.md)