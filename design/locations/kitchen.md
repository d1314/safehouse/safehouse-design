# Kitchen

#kitchen #location 

## Concept art

(Camera position "Kitchen")

![](../images/concept_kitchen.png)

## Description

We see a very functional kitchen with a [kitchen_fridge](../items/kitchen_fridge.md), a [kitchen_left_cupboard](../items/kitchen_left_cupboard.md), a [kitchen_right_cupboard](../items/kitchen_right_cupboard.md) and a [kitchen_stove](../items/kitchen_stove.md). A [kitchen_hatch](../items/kitchen_hatch.md) on the opposing wall directly connects to the [living_room](living_room.md).

An invisible door in the foreground leads back to the [living_room](../ambients/living_room.md). The [door_storeroom](../items/door_storeroom.md) in the back leads to a store room, but is locked.

There's a window here and we see the root of a tree in the garden.

We need a night and a day variant.

## SFX

Walking plays [footsteps_tiles](../sfx/footsteps_tiles.md)

## Ambient

- [kitchen](kitchen.md)