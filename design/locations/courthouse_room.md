# Courthouse room

#location #courthouse_room

## Concept art

https://nytaxattorney.com/consultations-2/conference-room-3/

https://www.dreamstime.com/law-library-conference-room-oklahoma-city-judiciary-building-office-courts-image180870735

## Description

The court room is a backdrop for the epilogue scene between [marjorie](../characters/marjorie.md) and [brianna](../characters/brianna.md). It symbolizes safety. It should not only make [marjorie](../characters/marjorie.md) feel safe, but the player as well. It is grand and old and emits nobility. It's pretty much styled like the courthouse meeting rooms in "Law & Order"

There are bookshelves filling the walls.

There's also a meeting room in the courthouse. The walls are made of oak wood, there's a dark brown carpet on the floor.

## SFX

Walking plays [footsteps_carpet](../sfx/footsteps_carpet.md)

## Ambient

- [courthouse_room](courthouse_room.md)