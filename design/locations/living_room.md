# Living room

#location #livingroom 

## Concept art

(Camera position "Living room")

![](../images/concept_living_room.png)

## Description

The living room is the area where you usually can find [calvin](../characters/calvin.md) and [rebeca](../characters/rebeca.md). Also, the dialogs with [brianna](../characters/brianna.md) take place here.

It's a very simple room with a wooden floor.

The living room is directly connected to the [hall](hall.md) and to the [kitchen](kitchen.md) (via the [kitchen_door](../items/kitchen_door.md)).

On the left side there's a [living_room_table](../items/living_room_table.md), where the meals take place, so [day1_dishes](../items/day1_dishes.md), [day1_leftovers](../items/day1_leftovers.md), [day2_dishes](../items/day2_dishes.md), [day2_dishes](../items/day2_dishes.md), [day3_dishes](../items/day3_dishes.md), [salt](../items/salt.md) and [pepper](../items/pepper.md) and [bowl](../items/bowl.md) are placed on it.

On the back wall, there's a [living_room_sofa](../items/living_room_sofa.md), where the dialogs with [brianna](../music/brianna.md) take place. The sofa sits under a [living_room_hatch](../items/living_room_hatch.md) peeking into the [kitchen](kitchen.md). 

On the right wall is a TV and a [living_room_cupboard](../items/living_room_cupboard.md).

We need a night and a day variant.

## SFX

Walking plays [footsteps_wood](../sfx/footsteps_wood.md)

## Ambient

- Day: [Living room](../ambients/living_room.md#Living%20room)
- Night: [Living room night](../ambients/living_room.md#Living%20room%20night)