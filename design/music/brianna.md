# Brianna

#music 

Brianna's character represents the law and gives structure to the game.

The dialogs with Brianna act as an intersection and haven between the emotional parts of the game.

Thus the theme has some calming, but also "correct" and "structured" parts.

There's no other variation than the main theme played.