# Marjorie

#music

Marjorie's theme is caring and emotional. As Marjorie is the main character of the game, it should be the most "catchy" theme.

The following variations are required:

## Marjorie Relaxed

A calm, relaxed, downbeat play with the Marjorie theme.

## Marjorie Conflict

(Played on [DAY 1 - MORNING](../gdd.md#DAY%201%20-%20MORNING))

Starts with [Marjorie Relaxed](#Marjorie%20Relaxed), then gets tense and dramatic and culmulates when Rebeca finds Marjorie on the phone, then it underlines the shouting of Rebeca and the emotions of Marjorie ending with a dramatic Marjorie theme line and a hard stop.

## Marjorie Horror Strings

Strings sliding like from a horror movie. More a sound effect than a real tune.

## Marjorie Horror

[Marjorie Horror Strings](marjorie.md#Marjorie%20Horror%20Strings) merging into a rumbling and hectic Marjorie tune. It is played when Marjorie finds a noose in her room on [DAY 1 - EVENING](../gdd.md#DAY%201%20-%20EVENING) and alarms the others. Ultimatively, the noose is suddenly gone. Because of this, the tune ends with a rumbling bass with shriek Marjorie theme tunes and then stops hard.

## Marjorie Panic

Played very shortly in a nightmare and starts very sudden with a big culmination, very powerful and then sounds off into a reverb.

## Marjorie Night

A music representing a "night atmosphere". Very few notes, eerie, playful. Not scary though.

## Marjorie Scary Night

A more eerie and downright scary variation of [Marjorie Night](#Marjorie%20Night).

## Marjorie Gaining Confidence

Marjorie talks herself into more confidence. It starts with a soft Marjorie theme, then positively and angrily swells up and ends with a triumph and then cuts off (as Marjorie hits the light switch and thus ends the scene)

## Marjorie Triumph

A triumphant variant of the theme, played in the credits. Happy, confident and reprises the theme.