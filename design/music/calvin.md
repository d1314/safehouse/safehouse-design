# Calvin

#music 

Calvin is a two-sided character. On the surface, he's caring and helpful, but under the surface lies a deeply immoral and greedy persona.

The music should have very small hints at the second persona, because it is only revealed at the end.

So it should be a smooth, positive sound with some deep and some shrieking "mistunes".

## Calvin jazzy

The Calvin theme in a jazzy context. It's something you notice in the background, but don't really care about. The "mistunes" irritate the player a bit though.

## Calvin revealing

The music played, when the player realizes Calvin's second persona.

Calvin's theme, but with more accents on the "mistones". It starts slow though, as the revelation is some seconds after the start and it all goes down from there.

## Calvin chasing

We start with a big culmulation of the Calvin theme as Calvin apparently shoots Rebeca at that point. Then he realies Marjorie's presents and we continue with a rumbling "chase" variation.

## Calvin switching

Calvin is trying to get Marjorie and tries to sweet-talk to her. This definitely reveals the two personas, so we're moving back and forth between the positive sound and the "mistunes" in the theme.