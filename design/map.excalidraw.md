---

excalidraw-plugin: parsed

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Bedroom ^JWfLjd7F

Upper Hall ^96VZK05E

Hall ^urdAN62y

Living Room ^yO7HJuS6

Kitchen ^gIgWbiE0

Hatch ^0pUpzudO

Safehouse - Room Map ^1Jkpzipd

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "rectangle",
			"version": 316,
			"versionNonce": 304347317,
			"isDeleted": false,
			"id": "hOg4HF_uwWaKgauJeTgVK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -93.34691305721503,
			"y": 88.18938490923699,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 270.3106689453125,
			"height": 161.90167236328125,
			"seed": 523430779,
			"groupIds": [
				"kU2SkKgE_OOw_8-BLp3OW"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "soIixkI-FO6YdQFrNPWVC",
					"type": "arrow"
				},
				{
					"id": "QSdpFMnoBFUyPrWKB1bYu",
					"type": "arrow"
				}
			],
			"updated": 1642150675987
		},
		{
			"type": "text",
			"version": 200,
			"versionNonce": 213702939,
			"isDeleted": false,
			"id": "JWfLjd7F",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 0.0728257123162166,
			"y": 150.17260024126824,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 80,
			"height": 25,
			"seed": 1180356923,
			"groupIds": [
				"kU2SkKgE_OOw_8-BLp3OW"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642150675987,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Bedroom",
			"rawText": "Bedroom",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Bedroom"
		},
		{
			"type": "rectangle",
			"version": 116,
			"versionNonce": 1290607125,
			"isDeleted": false,
			"id": "xN82bTCrkzUlURimsIfBq",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -96.7840576171875,
			"y": -172.37469482421875,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 276.8095703125,
			"height": 176.18341064453125,
			"seed": 56955931,
			"groupIds": [
				"9ywFi0pVvBCrh0wtYDuer"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "QSdpFMnoBFUyPrWKB1bYu",
					"type": "arrow"
				},
				{
					"id": "kOE85WTa8HvN_5cR4EvMS",
					"type": "arrow"
				}
			],
			"updated": 1642150675987
		},
		{
			"type": "text",
			"version": 118,
			"versionNonce": 395902395,
			"isDeleted": false,
			"id": "96VZK05E",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -6.3062744140625,
			"y": -98.8125,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 98,
			"height": 25,
			"seed": 584542037,
			"groupIds": [
				"9ywFi0pVvBCrh0wtYDuer"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642150675987,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Upper Hall",
			"rawText": "Upper Hall",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Upper Hall"
		},
		{
			"type": "rectangle",
			"version": 215,
			"versionNonce": 147525493,
			"isDeleted": false,
			"id": "TslNO6QumIs552cKC8Zkm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -237.1816585765166,
			"y": -454.59923239315253,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 294,
			"height": 187.91314697265625,
			"seed": 2102463637,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "urdAN62y"
				},
				{
					"id": "kOE85WTa8HvN_5cR4EvMS",
					"type": "arrow"
				},
				{
					"id": "HjIDYuJYUyjOxa98Tr93I",
					"type": "arrow"
				}
			],
			"updated": 1642150675987
		},
		{
			"type": "rectangle",
			"version": 287,
			"versionNonce": 1256170011,
			"isDeleted": false,
			"id": "6FB2JIkzOGlLBF8z0UC2v",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -509.7146175608916,
			"y": -189.47704001034003,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 294,
			"height": 187.91314697265625,
			"seed": 317425179,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "yO7HJuS6",
					"type": "text"
				},
				{
					"id": "kOE85WTa8HvN_5cR4EvMS",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "yO7HJuS6"
				},
				{
					"id": "HjIDYuJYUyjOxa98Tr93I",
					"type": "arrow"
				},
				{
					"id": "0fTpMap7DWOSVZIX4lcvT",
					"type": "arrow"
				}
			],
			"updated": 1642150682386
		},
		{
			"type": "rectangle",
			"version": 484,
			"versionNonce": 298140155,
			"isDeleted": false,
			"id": "pKv23n6ikOWoktfRoblyP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -598.0761898265166,
			"y": 74.25550393497247,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 294,
			"height": 187.91314697265625,
			"seed": 164325115,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "gIgWbiE0",
					"type": "text"
				},
				{
					"id": "kOE85WTa8HvN_5cR4EvMS",
					"type": "arrow"
				},
				{
					"id": "gIgWbiE0",
					"type": "text"
				},
				{
					"id": "HjIDYuJYUyjOxa98Tr93I",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "gIgWbiE0"
				},
				{
					"id": "0fTpMap7DWOSVZIX4lcvT",
					"type": "arrow"
				}
			],
			"updated": 1642150686873
		},
		{
			"type": "text",
			"version": 190,
			"versionNonce": 825870227,
			"isDeleted": false,
			"id": "urdAN62y",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -232.1816585765166,
			"y": -373.1426589068244,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 284,
			"height": 25,
			"seed": 1925251189,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642457180461,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Hall",
			"rawText": "Hall",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "TslNO6QumIs552cKC8Zkm",
			"originalText": "Hall"
		},
		{
			"type": "text",
			"version": 270,
			"versionNonce": 1271991261,
			"isDeleted": false,
			"id": "yO7HJuS6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -504.7146175608916,
			"y": -108.02046652401191,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 284,
			"height": 25,
			"seed": 134987029,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642457180464,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Living Room",
			"rawText": "Living Room",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "6FB2JIkzOGlLBF8z0UC2v",
			"originalText": "Living Room"
		},
		{
			"type": "text",
			"version": 473,
			"versionNonce": 1674307091,
			"isDeleted": false,
			"id": "gIgWbiE0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -593.0761898265166,
			"y": 155.7120774213006,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 284,
			"height": 25,
			"seed": 1480514101,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642457180467,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Kitchen",
			"rawText": "Kitchen",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "pKv23n6ikOWoktfRoblyP",
			"originalText": "Kitchen"
		},
		{
			"type": "arrow",
			"version": 328,
			"versionNonce": 807987093,
			"isDeleted": false,
			"id": "QSdpFMnoBFUyPrWKB1bYu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 42.08629997223895,
			"y": 85.49498793658074,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1.7771390332127837,
			"height": 79.99773451861199,
			"seed": 1345393845,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1642150675987,
			"startBinding": {
				"elementId": "hOg4HF_uwWaKgauJeTgVK",
				"focus": -0.01154385815946587,
				"gap": 2.69439697265625
			},
			"endBinding": {
				"elementId": "xN82bTCrkzUlURimsIfBq",
				"focus": -0.030187484881920846,
				"gap": 1.68853759765625
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					1.7771390332127837,
					-79.99773451861199
				]
			]
		},
		{
			"type": "arrow",
			"version": 84,
			"versionNonce": 77570653,
			"isDeleted": false,
			"id": "kOE85WTa8HvN_5cR4EvMS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 143.18663200669806,
			"y": -174.63097605985752,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 130.0660511712062,
			"height": 89.41768870634189,
			"seed": 2113733755,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1642457180458,
			"startBinding": {
				"elementId": "xN82bTCrkzUlURimsIfBq",
				"gap": 2.2562812356387667,
				"focus": 0.8741027573744601
			},
			"endBinding": {
				"elementId": "TslNO6QumIs552cKC8Zkm",
				"gap": 2.637420654296875,
				"focus": 0.13114752800519552
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-130.0660511712062,
					-89.41768870634189
				]
			]
		},
		{
			"type": "arrow",
			"version": 115,
			"versionNonce": 1415666995,
			"isDeleted": false,
			"id": "HjIDYuJYUyjOxa98Tr93I",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -146.20467264096646,
			"y": -265.6860854204963,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 117.64526466130684,
			"height": 72.78972850126377,
			"seed": 1654410901,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1642457180463,
			"startBinding": {
				"elementId": "TslNO6QumIs552cKC8Zkm",
				"gap": 1,
				"focus": -0.3260735448758225
			},
			"endBinding": {
				"elementId": "6FB2JIkzOGlLBF8z0UC2v",
				"gap": 3.4193169088924833,
				"focus": -0.19580538877001516
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-117.64526466130684,
					72.78972850126377
				]
			]
		},
		{
			"type": "arrow",
			"version": 255,
			"versionNonce": 1222477885,
			"isDeleted": false,
			"id": "0fTpMap7DWOSVZIX4lcvT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -278.6864451606307,
			"y": 0.4968315573299691,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 25.643090644045003,
			"height": 71.442626953125,
			"seed": 1489882651,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1642457180466,
			"startBinding": {
				"elementId": "6FB2JIkzOGlLBF8z0UC2v",
				"gap": 2.0607245950137667,
				"focus": -0.6556509303812174
			},
			"endBinding": {
				"elementId": "pKv23n6ikOWoktfRoblyP",
				"gap": 2.3160454245174833,
				"focus": 0.6207876021786296
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-25.643090644045003,
					71.442626953125
				]
			]
		},
		{
			"type": "rectangle",
			"version": 73,
			"versionNonce": 1309552219,
			"isDeleted": false,
			"id": "yHVVKVUINuirKg5Aym9wj",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -479.68054019703595,
			"y": -14.109796860638767,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 30.53326416015625,
			"height": 100.36663818359375,
			"seed": 1830658011,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642150692569
		},
		{
			"type": "text",
			"version": 124,
			"versionNonce": 1182051893,
			"isDeleted": false,
			"id": "0pUpzudO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 4.691042261548004,
			"x": -493.22155582203595,
			"y": 22.364263197954983,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 56,
			"height": 25,
			"seed": 1720560379,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642150707159,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Hatch",
			"rawText": "Hatch",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Hatch"
		},
		{
			"type": "text",
			"version": 100,
			"versionNonce": 700240923,
			"isDeleted": false,
			"id": "1Jkpzipd",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -294.38451968922345,
			"y": -615.8024543313419,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 406,
			"height": 45,
			"seed": 149426235,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1642150733247,
			"fontSize": 36,
			"fontFamily": 1,
			"text": "Safehouse - Room Map",
			"rawText": "Safehouse - Room Map",
			"baseline": 32,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Safehouse - Room Map"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 36,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null
	},
	"files": {}
}
```
%%