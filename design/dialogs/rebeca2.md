# Rebeca 2

#dialogs #livingroom 

# Comment

This dialog takes place after Marjorie has found the fake diary of Jacob Grimes. She wants to ask Rebeca about her find (Calvin is not around at this time). Rebeca doesn't reveal much, other than that the diary belongs to one "Jacob Grimes", and that he was found dead eighteen years ago.

However, during the course of the dialog, Marjorie reveals the contents of the diary to Rebeca. She is interested, takes it and reads pieces of it afterwards. She then cuts off the dialog.

# Start

> [marjorie](../characters/marjorie.md)
> 
> Do you know where Calvin is?

> [rebeca](../characters/rebeca.md)
>
> No. Didn't see him.

> [marjorie](../characters/marjorie.md)
>
> Oh, okay.

Seconds pass.

> [rebeca](../characters/rebeca.md)
>
> (sighs) What is it?

> [marjorie](../characters/marjorie.md)
>
> I... Does the name JAYGEE mean anything to you?

> [rebeca](../characters/rebeca.md)
>
> You mean like initials?

> [marjorie](../characters/marjorie.md)
>
> No, JAYGEE. J - A - Y - G - Double E.

> [rebeca](../characters/rebeca.md)
>
> (beat) Where did you hear that name?

> [marjorie](../characters/marjorie.md)
>
> I didn't hear it. I read it.

> [rebeca](../characters/rebeca.md)
>
> What?

> [marjorie](../characters/marjorie.md)
>
> So it does ring a bell.

> [rebeca](../characters/rebeca.md)
>
> Damn right it does. Where did you read it?

> [marjorie](../characters/marjorie.md)
>
> Who is that?

> [rebeca](../characters/rebeca.md)
>
> Was.

> [marjorie](../characters/marjorie.md)
>
> What?

> [rebeca](../characters/rebeca.md)
>
> Who *was* that. His name was Jacob Grimes. Where did you read about him?

> [marjorie](../characters/marjorie.md)
>
> Is he dead?

> [rebeca](../characters/rebeca.md)
>
> Marjorie, where did you read about him?

> [marjorie](../characters/marjorie.md)
>
> I found this in a locked drawer in my room.

[marjorie](../characters/marjorie.md) hands [rebeca](../characters/rebeca.md) the [diary](../items/diary.md).

Remove [diary](../items/diary.md)

> [rebeca](../characters/rebeca.md)
>
> (to herself, in thought) What the fuck?

> [marjorie](../characters/marjorie.md)
>
> So he *is* dead?

> [rebeca](../characters/rebeca.md)
>
> (absent-minded) Died eighteen years ago.

> [marjorie](../characters/marjorie.md)
>
> That's terrible. How did he die?

(pause)

> [marjorie](../characters/marjorie.md)
>
> Rebeca?

> [rebeca](../characters/rebeca.md)
>
> Huh?

> [marjorie](../characters/marjorie.md)
>
> How did he die?

> [rebeca](../characters/rebeca.md)
>
> That can't be...

> [marjorie](../characters/marjorie.md)
>
> What?

> [rebeca](../characters/rebeca.md)
>
> (snaps out of it) Sorry, Marjorie. I can't tell you anything else. Can you please leave me alone?

> [marjorie](../characters/marjorie.md)
>
> But...

> [rebeca](../characters/rebeca.md)
>
> *Please*, Marjorie.

> [marjorie](../characters/marjorie.md)
>
> Um... okay...

End