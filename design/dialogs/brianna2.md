# Brianna 2

#dialogs #livingroom 

# Comment

In this dialog, the main topic is the death of Jacob Grimes. Marjorie has searched online about it, but hasn't got any details. She wants to know the official version.

Brianna can't give her all the details because of legal matters. However, Marjorie succeeds on getting at least _some_ details.

It is pointed out, that:

* Jacob was a lowlife gang banger, accused of stealing, doing graffiti
* Jacob saw Igino Varone killing of Derek Cole, an opposing drug dealer
* Jacob spent four days in the house until he was found hung in his bedroom (which is Marjorie's room now)
* The bedroom was locked
* Calvin was on that job
* Everybody assumed the pressure drove Jacob to suicide, but it wasn't apparent before the event

# Start

> [brianna](../characters/brianna.md)
>
> Marjorie! Nice to see you again. How was your first night?

> [marjorie](../characters/marjorie.md)
>
> ...

> [brianna](../characters/brianna.md)
>
> Marjorie?

> [marjorie](../characters/marjorie.md)
>
> (dark) It was... (beat) okay.

> [brianna](../characters/brianna.md)
>
> Are you alright, Marjorie?

> [marjorie](../characters/marjorie.md)
>
> Can I ask you something?

> [brianna](../characters/brianna.md)
>
> Of course.

> [marjorie](../characters/marjorie.md)
>
> Do you know about Jacob Grimes?

> [brianna](../characters/brianna.md)
>
> (shocked) (beat)
>
> (haltingly) I know his case, yes.

> [marjorie](../characters/marjorie.md)
>
> You should've told me about him.

> [brianna](../characters/brianna.md)
>
> Marjorie...

> [marjorie](../characters/marjorie.md)
>
> (interrupting, shouting) You should've fucking told me about him!

> [brianna](../characters/brianna.md)
>
> That was 18 years ago!

> [marjorie](../characters/marjorie.md)
>
> (still shouting) He was killed by the same people you want me to testify against!

> [brianna](../characters/brianna.md)
>
> (getting louder) He killed himself!

> [marjorie](../characters/marjorie.md)
>
> (still shouting) They made him do that!

> [brianna](../characters/brianna.md)
>
> (shouting) No! We did that!

*pause*

> [brianna](../characters/brianna.md)
>
> (quiet again) We didn't know about his condition.

> [marjorie](../characters/marjorie.md)
>
> What do you mean? Condition?

> [brianna](../characters/brianna.md)
>
> He was... He was too stressed out and we didn't realize that. We kept pushing and pushing him because we wanted to have a solid case against La Famiglia for once.
>
> But we pushed too much. And he couldn't take it anymore.

*pause*

> [marjorie](../characters/marjorie.md)
>
> What was that case about?

> [brianna](../characters/brianna.md)
>
> I'm not allowed to tell you...

> [marjorie](../characters/marjorie.md)
>
> (interrupts) Brianna, please, I need to know.

*pause*

> [brianna](../characters/brianna.md)
>
> (sigh) Okay. What do you want to know?

[Questions](#Questions)

# Questions

* [I take it that Jacob was in witness protection as well?](#witnessprotection)
* If asked previous question: [Was it about Varone as well?](#varone)
* [Jacob hung himself?](#suicide)
* [Thank you for sharing, Brianna.](#end)

# witnessprotection

> [brianna](../characters/brianna.md)
>
> Yes, he was. He was kept in this house actually. It was the first time we used it as a safehouse.

> [marjorie](../characters/marjorie.md)
>
> So he... He stayed in the very same room I'm in?

> [brianna](../characters/brianna.md)
>
> (realizes) I... I guess so, yes. Oh god, Marjorie.

> [marjorie](../characters/marjorie.md)
>
> (pause, then confident) It's okay.

[Questions](#Questions)

# varone

> [brianna](../characters/brianna.md)
>
> (dark) It's always about him.
>
> Jacob was a lowlife, a cheap gangbanger. A street kid who took the wrong turn.
>
> There was this drug dealer in his gang... (thinks) Derek. Yes, Derek Cole was his name. And well, he was in an area that belongs to La Famiglia.

> [marjorie](../characters/marjorie.md)
>
> Bad idea.

> [brianna](../characters/brianna.md)
>
> Bad idea.
>
> Jacob saw Varone... taking care of the problem.

> [marjorie](../characters/marjorie.md)
>
> (whispering) Oh fuck.

[Questions](#questions)

# suicide

> [brianna](../characters/brianna.md)
>
> (shocked from Marjorie's directness) Y-yes, yes he did.
>
> (calms herself) Calv... I mean the police officers found him hanging in his bedroom on the day of the trial.

> [marjorie](../characters/marjorie.md)
>
> I've already read that Calvin was involved.

> [brianna](../characters/brianna.md)
>
> He was devastated.

> [marjorie](../characters/marjorie.md)
>
> I believe so.

*pause*

> [marjorie](../characters/marjorie.md)
>
> But Jacob did definitely kill himself?

> [brianna](../characters/brianna.md)
>
> Yes. Calvin had to break his door when he wasn't responding.

[Questions](#questions)

# end

> [brianna](../characters/brianna.md)
>
> Don't tell anyone, please. This could get me in trouble.

> [marjorie](../characters/marjorie.md)
>
> I swear.

> [brianna](../characters/brianna.md)
>
> Great. Let's continue preparing you for the trial, shall we?

> [marjorie](../characters/marjorie.md)
>
> (sigh) Okay.

END
