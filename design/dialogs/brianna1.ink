/* Brianna 1

This non-interactive dialog shows Brianna introducing herself and going through Marjorie's testimony. It depicts the extreme stress Marjorie is in, and how horrific the crime she has witnessed was.

Beats:  

* Brianna wants to get to know Marjorie and make sure, she doesn't loose her shit in court
* Marjorie is stressed out and tense
* The wounds from the events Marjorie had to go through haven't healed at all
* Marjorie wants to follow this through though, because she thinks, it's the right thing to do
*/

brianna: Marjorie, I'm Brianna Alison. I'm the district attorney and will lead the case against Igino Varone.

* [Hello.]

    marjorie: (sigh) Hello Brianna.
    
    brianna: I guess it's not easy.
    
    -> holding_up

* [I know who you are.]

    -> calvin_told_me
    
* [Can we get this over with quickly?]

    marjorie: Hello. Can we get this over with quickly?
    
    brianna: (determined) Yes, of course.
    
    -> the_story

== calvin_told_me ==

marjorie: Yes, I know who you are. Calvin told me that you'd come.

brianna: I see.

(friendly) Well. <>-> holding_up

== holding_up ==

{ calvin_told_me: 

brianna: Have you settled in?

marjorie: Well... yes. I guess. It's... It is like it is.

brianna: Yes, I understand. It's not easy.

}

* [No.]

    marjorie: No, it isn't.
    
    -> manage

* [Hell no!]

    marjorie: Hell no!
    
    -> manage

* [I got this.]

    marjorie: Doesn't matter. I got this. 
    
    -> got_this

= manage

brianna: But you manage?

marjorie: I... (reassuring herself) Yes, I got this.

-> got_this

= got_this

brianna: Great. Thank you so much for your support.

marjorie: I hope it helps.

brianna: Oh, it definitely does. -> the_story

== the_story ==

= starting

brianna: So, if it's okay with you, can we {|now} go back to last Friday{! please}?

* [Yes.]

    marjorie: Yes, okay.
    -> tell

* {starting == 1}[Do we have to?]

    marjorie: Do we have to?
    
    brianna: -> stalling

* {starting == 1}[I don't want to!]

    marjorie: I... I don't want to do this.
    
    brianna: Marjorie, we need to prepare you for court or you won't be ready when the defense hits you.
    -> stalling

= stalling
    
brianna: I know, how hard this is for you.
    
marjorie: (shouts) You don't have a fucking clue!

brianna: (calming her) Yes, of course. But I understand it. I've supported many clients on their way.

marjorie: (calms down) I guess, they weren't a mess like I am.

brianna: (firm) Actually. They were even more broken than you are.

marjorie: They were?

brianna: Yes.

(pause)

-> starting

= tell

brianna: {not stalling:  I know it's hard, Marjorie. But c|C}an you please tell me what happened?

marjorie: Again?

brianna: Yes. I have to prepare you for the trial. You will have to repeat it there as well and be prepared for questions from the defense attorney.

marjorie: (sighs)

brianna: Yes, I know. I will try to protect you at all times, but there will be questions the judge will allow that you have to answer. Do you understand?

marjorie: Yes. I guess.

-> continue

== continue ==

brianna: Okay, so what happened Friday night?

marjorie: Well, I was out of milk and went to the Shop-O down the street.

brianna: What time was it?

* [6 to 6: 30 pm.]

    marjorie: 6 to 6: 30 pm
    
    brianna: 6 or 6: 30 pm?
    
    -> wrong_time

* [7 to 7: 30 pm.]

    marjorie: 7 to 7: 30 pm
    
    brianna: 7 or 7: 30 pm?
    
    -> pinpoint

* [7: 30 to 8 pm.]

    marjorie: 7: 30 to 8 pm
    
    brianna: 7: 30 or 8pm?
    
    -> pinpoint

= wrong_time

-> pinpoint

= pinpoint
    
brianna: Can you pinpoint the incident more exactly? The time frame might prove to be important.

marjorie: {wrong_time:  Oh. No, sorry. }It was 7: 30. I watched a comedy show I regularly watch and it had just finished, so I was on the street at seven thirty pm. Five minutes to the Shop-O.

brianna: Good, Continue.

marjorie: I walked into the shop and said hi to... To Jazir.

brianna: Jazir Bijarani? The man who owned the shop?

marjorie: (sad) Yes. Jazir.

brianna: Okay, please continue.

-> stalling2

== stalling2 ==

* {stalling2.did_stall == 0}[(Say nothing)]

    marjorie: (...)
    
    brianna: Marjorie?
    
    marjorie: (startles) Yes?
    
    -> did_stall

* {stalling2.did_stall == 0}[Poor Jazir.]

    marjorie: Poor Jazir.
    
    brianna: (soft) Oh, Marjorie.
    
    (pause)
    
    Can we continue?
    
    -> did_stall
    
* [I went to the fridge.]

    -> continue2

= did_stall

-> stalling2
    
== continue2 ==

marjorie: I went to the fridge to get some milk when I heard an argument over at the counter.

{ not stalling2.did_stall: 

brianna: You're doing great, Marjorie.

}

marjorie: There... I saw Jazir with the guy they arrested.

brianna: Do you know his name?

* [I don't remember.]

    marjorie: I don't remember.
    
    brianna: Please, take your time.
    
    marjorie: (beat) Varone.

* [I don't want to say his name.]

    marjorie: I can't say his fucking name.
    
    brianna: Marjorie, please. You need to brace yourself for court.
    
    marjorie: (to herself) Fuck.
    
    marjorie: (aloud) Varone.

* [I know his name.]
    
    marjorie: Varone.

-

brianna: Please say his full name.

marjorie: (sighs) Igino Varone. He was shouting at Jazir. He was furious. I guess, he didn't know I was around.

brianna: What were they arguing about?

marjorie: I don't know. This Varone guy kept shouting:  "You think you can fuck with me? You really think you can fuck with me". Those kinds of words.

brianna: And Jazir?

marjorie: He was terrified. I mean, we all know who Igino Varone is. You don't want him shouting at you.

brianna: What do you mean that you all know who he is?

* (killer)[A killer.]

    marjorie: He's a killer. Everybody knows that!

* (drug_dealer)[A drug dealer.]

    marjorie: He's selling drugs. Everybody knows that!

* [He's "La famiglia".]

    marjorie: He's "La Famiglia". He's doing all sorts of stuff. Killing people, selling drugs, prostitution...

-

brianna: Marjorie, we don't have any proof of those kind of things.

marjorie: {killer || drug_dealer:  But he's "la famigila"! E|But e}verybody knows he does that!

brianna: (calming her down) Maybe, but we have to concentrate on this specific case for which we have *your* word and a few dozen clues. Okay?

marjorie: Okay.

brianna: Great. Thank you. So Mr. Bijarani was terrified.

marjorie: Yes. And he said, that he didn't know what Varone was talking about. And then...

(pause)

-> stalling3

== stalling3 ==

* [(Say nothing)]

    marjorie: (...)
    
    brianna: Marjorie?
    
    marjorie: (sigh)
    
    -> stalling3

* [I don't want to go on.]

    marjorie: I don't want to go on. Do I have to go on?
    
    brianna: You're doing great, Marjorie. Please, yes.
    
    -> stalling3

* [All of a sudden...]

-> continue3

== continue3 ==

marjorie: All of a sudden... he drew a gun and... pointed at him, and I quickly hid behind a shelf.

brianna: How far was this shelf away from the counter?

marjorie: I don't know. It was the second row in the back where the fridges were. I guess... I guess maybe 30 feet or something.

brianna: Okay, go on.

marjorie: So I hid and Varone kept on shouting and then... (pauses)

brianna: Should we take a break?

* [Yes.]

    brianna: Okay. Relax. Breathe.
    
    marjorie: (breathes in)
    
    (breathes out)
    
    (drinks)
    
    brianna: Do you think, you can go on?
    
    marjorie: I... Yes, I think so.
    
    brianna: So what happened then?

* [No.]

    marjorie: No, it's... It's fine. Thank you.
    
    brianna: Okay.

-

marjorie: Then he shot. BAM BAM BAM. Three times. (sobbing) He shot Jazir three times. There was blood everywhere.

brianna: What did you do then?

marjorie: (still sobbing) I was in shock. I still hid behind the shelf. I didn't (sob) scream, I... why didn't I scream? I lost track of time. Then I looked to the counter again, but Varone was gone. I went to (sob) I went to Jazir and... all this blood. (she cries)

(pause)

brianna: Can you continue?

marjorie: (still sobbing, but getting herself together) Yes... yes, okay. (breath) Jazir wasn't breathing anymore, so I took the phone and called 911.

brianna: And then?

* [What do you mean?]
    
    marjorie: What do you mean?
    
    brianna: You have to tell things exactly and completely as they were. If you leave things out, the defense will rip you open on those.
    
    marjorie: Okay I... I freaked out. It was all too much, you know?

* [Nothing!]

    marjorie: Nothing! He was dead!
    
    brianna: Remember to recount things exactly and completely as they were. If you leave things out, the defense will rip you open on those.
    
    marjorie: (angry) Fine. I freaked out. It was all too much, you know?

* [I freaked out.]

    marjorie: I freaked out. It was all too much, you know?

-

brianna: Yes, I understand. What did you do?

marjorie: I just ran. I ran home and locked my doors. I expected to get shot any second. I only opened when the police knocked at my door when they had started questioning the neighborhood.

brianna: Okay. I think, they will ask you why you didn't stay and help Jazi

marjorie: (interrupts shouting) He was fucking dead!

brianna: (Trying to calm her down) Yes, I know. I can understand that. Please calm down.

marjorie: (still shouting) I saw him die!

marjorie: (pause)

marjorie: Fuck!

marjorie: (pause)

marjorie: They will ask something like that?

brianna: Look, you're the witness. That makes you the defense's enemy. If they can't prove that you're lying, they have to render you unreliable.

marjorie: Okay.

brianna: The best thing is to stay calm, and I know how hard that is. Stay calm and answer their questions truthfully.

marjorie: Okay. I'll try.

brianna: I know. Thank you so much for helping us!

marjorie: Okay.

-> END
