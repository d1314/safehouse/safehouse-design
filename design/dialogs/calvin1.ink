/* Calvin1

Marjorie is feeling guilty about the phone incident and wants to apologize to Calvin and expects a kind reaction as opposed to apologizing to Rebeca. She needs this kind reaction to ease her mind.

Calvin is already planning how he can scare Marjorie so she doesn't testify. However, he puts a lot of effort into keeping up his facade, so that nobody suspects him. So he fakes, that he wants to help and comfort Marjorie.

*/

// Wether Marjorie has looked at the padlock
VAR has_seen_padlock = true

->intro

== intro

MARJORIE

(tense) Hey, Calvin.

CALVIN

(friendly) Marjorie, hello. How are you doing?

MARJORIE

I'm... okay, I guess.

CALVIN

Ah, that's good.

MARJORIE

(pause)

I... (breathes) Calvin, I like to apologize for my behaviour.

CALVIN

Marj...

MARJORIE

(interrupts) I shouldn't have bring my telephone, that was dumb.

I'm sorry.

CALVIN

Marjorie, it's okay. This is a pretty hard situation for you I can imagine. I can totally understand, that you need some support from your family to help you manage.


MARJORIE

Yes.

CALVIN

It's just very important, that we isolate you from the outside world until the end of the week. That way we can keep you safe, you understand?

MARJORIE

Yes, of course. I hope I didn't put myself in danger.

CALVIN

Ah, I don't think so. I mean, Rebeca took care of the situation quickly. (laughs)

MARJORIE

(ashamed, but softly laughing) Yes, she did.

-> questions

== start

~ temp greeting = RANDOM(1, 3) 

{ greeting == 1:
    MARJORIE

    Can I ask you something, Calvin?

    CALVIN

    Sure. What's on your mind?
}
{ greeting == 2:
    MARJORIE

    Calvin?

    CALVIN

    Yes, Marjorie?
}
{ greeting == 3:
    MARJORIE

    Do you have a second, Calvin?

    CALVIN

    Absolutely. What's on your mind?
}

-> questions

== questions

* [How does this work?]

    -> howdoesthiswork
    
* [About this house.]
    
    -> house
    
* {has_seen_padlock == true} [About the padlock.]

    -> padlock
    
* [Thanks]

    MARJORIE
    
    Thank you, Calvin.
    
    CALVIN
    
    Anytime. If you need something, we're here for you.
    
    ->END

* ->END

== howdoesthiswork

MARJORIE

So how does this work? What do we do?

CALVIN

Well, we sit and wait.

MARJORIE

Nothing more?

CALVIN

Nothing more.

(beat)
 
Well, in the best case at least. Let's just hope nothing happens. (to himself) Unlike last time.

MARJORIE

Last time?

CALVIN

Hm?

MARJORIE

You said, you hope, that nothing happens *unlike last time*?

CALVIN

I did?

MARJORIE

Yes.

CALVIN

I... I meant... Look, Marjorie. This is a tense situation. We just stay put and wait until the trial and then it's over. Easy as that.

MARJORIE

Okay.

-> questions

== house

MARJORIE

Do you often use this house?

CALVIN

Not often, but sometimes. We have several objects like this. How do you like it?

* [Neat.]

    MARJORIE
    
    It's neat.
    
    CALVIN
    
    (laughs) Neat. Yes.. .Yes, I guess it is neat. Never thought of it this way, actually. I'm only here, when I'm on the clock.
    
* [Cold.]

    MARJORIE
    
    It feels... cold.
    
    CALVIN
    
    Huh. Yeah. Yeah, it's a bit sterile? The bare minimum.
    
* [I don't know.]

    MARJORIE
    
    It's just a house, I guess.
    
    CALVIN
    
    Well, yes. We try to make it comfortable for the witnesses though.
    
    MARJORIE
    
    It's okay, I guess.
    
    CALVIN
    
    Hm. Apparently we didn't really succeed with you. We'll try to do better.
    
-

-> questions

== padlock

MARJORIE

Why is there a padlock on one of the drawers in my room?

CALVIN

A padlock?

MARJORIE

Yes.

CALVIN

That's strange.

MARJORIE

Indeed.

CALVIN

Well, I don't know the combination, if that's what you're asking.

MARJORIE

Hm. Okay. Thanks.

-> questions
