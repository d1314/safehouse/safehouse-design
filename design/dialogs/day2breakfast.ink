/* day2breakfast

The group is eating breakfast after what happened last evening. There's a high tension in the room.

Marjorie is shy, nervous and tense. She still has a bad conscience about the phone incident. Additionally, she's concerned about her safety from Igino Varone and La Famiglia.

Because of the situation last evening, Rebeca is more mean, tense and angry than usual.

Calvin is calm and tries to bring down the tension that is in the room. He's nice and understanding to Marjorie.

When the scene starts, Marjorie tries to break the silence.

*/

// The player has asked either Brianna or Calvin about the padlock on day 1
VAR has_asked_about_padlock = false

MARJORIE

(while chewing toast) And? How was your night?

CALVIN

It was allright. The bed could use a new mattress.

How was yours?

* [I slept like a rock.]

    MARJORIE
    
    I didn't think I could sleep after... what happened.
    
    But I slept like a stone. This is all pretty exhausting.
    
    CALVIN
    
    Yes, I get that. But that's good. A good night's sleep is really helpful.
    
    MARJORIE
    
    I agree.
    
* [I couldn't sleep.]

    MARJORIE
    
    (grunt) I couldn't sleep. Not after... what happened.
    
    CALVIN
    
    Yes, I get that. You should try to lie down again later. Relax and refresh.
    
    MARJORIE
    
    (yawns) That's a good idea. I'll try to.

* [Too soft.]

    MARJORIE
    
    Mine was too soft. My back hurts.
    
    CALVIN
    
    Yes, I can relate. I will report that after we're done here.

-
    
-> questions

== questions ==

* [Did you arrest Varone?]

    -> varone
    
* [What if La Famiglia knows?]

    -> lafamiglia
    
* [About Brianna.]

    -> brianna

* {not has_asked_about_padlock and questions == 4} [What a strange padlock.]

    -> padlock

* {(has_asked_about_padlock and questions == 4) or (not has_asked_about_padlock and questions == 5)} [I'm hungry.]

    -> hungry

== varone ==

MARJORIE

Was any of you around when Varone got arrested?

CALVIN

Not me.

REBECA

I was there. I was in the squad.

-> arrest

= arrest

* [Exciting!]

    MARJORIE
    
    That sounds exciting!
    
    REBECA
    
    Eh, pretty standard job, I'd say. It's not like raiding a drug house. Everything's nice and clean and you're greeted by their lawyer the minute you knock on the door.
    
* [Was he afraid?]

    MARJORIE
    
    He wasn't afraid or anything?
    
    REBECA
    
    (hard laugh) Varone? That guy? That guy isn't afraid of anything. Not with all the lawyers, his (making fun of the name) "La Famiglia" suckers and bribed cops.
    
    CALVIN
    
    You don't know that.
    
    REBECA
    
    Pff.
    
* [Good thing, he's out of the way now.]

    MARJORIE
    
    Thank god! At least he's locked up now.
    
    REBECA
    
    Well, not anymore.
    
    MARJORIE
    
    (startled) He isn't?
    
    REBECA
    
    (laughs) Of course not.
    
    CALVIN
    
    Marjorie, people like Igino Varone have very good lawyers and can always find a loophole to stay out of jail. At least without a trial.
    
    MARJORIE
    
    (sad) Okay.
    
    REBECA
    
    That's where you come in.
    
    MARJORIE
    
    What?
    
    REBECA
    
    Key witness? Ring any bells?
    
    MARJORIE
    
    Oh, yes.

* -> questions

-

-> arrest

== lafamiglia ==

MARJORIE

What if "La Famiglia" knows about me?

REBECA

Oh, they know about you.

MARJORIE

(gasps)

CALVIN

Rebeca!

Marjorie, what Rebeca means is that they know there's a witness in the case. It's in the documents for the trial. However, they do not know who you are and certainly not where you are right now.

REBECA

Yeah, only six people know about your whereabouts. And three of them are sitting at this table.

CALVIN

Right.

MARJORIE

Who else?

CALVIN

Brianna, the judge and our chief.

MARJORIE

Okay. They sound reliable.

REBECA

Yes.

-> questions

== brianna ==

MARJORIE

Brianna is nice.

CALVIN

Agreed

REBECA

Yes. And professional. She's straight to the point.

-> aboutbrianna

= aboutbrianna

* [That's relieving.]

    MARJORIE
    
    That is reassuring, you know? To have a pro on that case. Makes me feel safe.
    
    CALVIN
    
    Absolutely, yes.

* [Is this her daily business?]

    MARJORIE
    
    Did she have many cases like this?
    
    CALVIN
    
    Oh yes, it's her job. NYC has some bad guys running around.
    
    REBECA
    
    Not anymore, thanks to her.
    
    CALVIN
    
    There are still a lot of bad guys running around.
    
    REBECA
    
    Whatever.
    
* [She's a good DA.]

    MARJORIE
    
    I guess she wins quite a few cases.

    REBECA
    
    If you don't buy her a drink.
    
    CALVIN
    
    Rebeca!
    
    MARJORIE
    
    What do you mean?
    
    CALVIN
    
    Nothing.
    
    MARJORIE
    
    (...)
    
    CALVIN
    
    Well, some people say, that Brianna had a bit of a drinking problem in her early years. But she managed.
    
    MARJORIE
    
    (...)
    
    CALVIN
    
    She's sober. For quite some time now. Don't worry.
    
    MARJORIE
    
    Okay.

* -> questions

- 

-> aboutbrianna

== padlock

MARJORIE

Did you notice the padlock on one of the drawers in my room?

CALVIN

Padlock? Can't say I have.

REBECA

I saw it yesterday.

MARJORIE

Do you know the combination?

REBECA

Nah. Never saw it before.

-> questions

== hungry ==

MARJORIE

Can you pass me the bacon, please?

REBECA

(...)

MARJORIE

Rebeca?

REBECA

Hm?

MARJORIE

Can you pass me the bacon please?

REBECA

Oh, sure.

-> END