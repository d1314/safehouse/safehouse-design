# Showdown

This dialog is the grand finale. We connect some of the loose ends and bring up the tension.

Throughout the showdown, the [Calvin switching](../music/calvin.md#Calvin%20switching) music is heard and we hear [door_bang](../sfx/door_bang.md).

After all dialog options have been asked, the showdown cutscene plays.

If no question is asked or no answer is played, we hear [calvin](../characters/calvin.md) shouting out:

- "(brutal) Let me in, bitch!"
- "(soft) Come on, Marjorie. I just want to talk!"
- "(brutal) Fuck it, bitch! Open up!"
- "(soft) Look, I've left my gun downstairs! I'm unarmed!"
- "(brutal) Open up that goddamn door!"
- "(soft) Okay! I leave you. (pause) (brutal) Let me fucking in!"


With every question answered, the [bedroom_wardrobe](../items/bedroom_wardrobe.md) gives more way:

- The [bedroom_wardrobe](items/bedroom_wardrobe.md) is shaking. Play [wardrobe_moving](sfx/wardrobe_moving.md)
- The [bedroom_wardrobe](items/bedroom_wardrobe.md) is moving. Play [wardrobe_moving](sfx/wardrobe_moving.md)
- The [bedroom_wardrobe](items/bedroom_wardrobe.md) moves some step away. The [bedroom_door](items/bedroom_door.md) is visible. Play [wardrobe_moving](sfx/wardrobe_moving.md)
- The [bedroom_wardrobe](items/bedroom_wardrobe.md) gives in, moves further away and the [bedroom_door](items/bedroom_door.md) swings open. Play [wardrobe_giving_in](sfx/wardrobe_giving_in.md)

# Questions

- "(shouting) I trusted you, Calvin!" [Trusted](#Trusted)
- "(shouting) You killed Rebeca!" [Rebeca](#Rebeca)
- "(shouting) Rebeca knew that you are a fucking bastard!" [Bastard](#Bastard)
- "(shouting) Did you kill Jacob too?" [Jacob](#Jacob)

After all questions have been asked:

- "(shouting) Why did you have to kill them?"

# Trusted

> [calvin](characters/calvin.md)
>
> (shouting) Yeah, like I said, I'm sorry!

# Rebeca

> [calvin](characters/calvin.md)
>
> (shouting) She was getting too close! She was getting suspicious...

# Bastard

> [calvin](characters/calvin.md)
>
> Well, a man's gotta provide for himself!

# Jacob

> [calvin](characters/calvin.md)
>
> Ahh, the kid.
> He just didn't get the message, that little punk.

# Killer

> [calvin](characters/calvin.md)
>
> Because there was no other way!

