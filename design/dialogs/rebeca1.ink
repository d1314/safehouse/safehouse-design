/* Rebeca1

Marjorie is feeling guilty and wants to apologize to Rebeca, but her shyness and insecurity gets in her way.

Marjorie needs her phone back and hopes, that Rebeca will return it to her.

(if Marjorie saw the padlock) Marjorie wants to know more about the padlock and if Rebeca knows something about it.

Rebeca is angry with Marjorie and she doesn't give in easily.

*/

// Wether Marjorie has looked at the padlock
VAR has_seen_padlock = true

// Wether Marjorie has made conversation with Rebeca
VAR made_conversation = false

-> intro

=== intro ===

MARJORIE

Rebeca?

REBECA

(...)

MARJORIE

Umm... This house... I didn't think, it would look like this.

REBECA

Uh-huh.

MARJORIE

I mean... It's a normal house.

REBECA

What did you think it would be? The Ritz?

* [Something more formal.]

    MARJORIE
    
    I would've thought, it would feel more... formal.
    
    REBECA
    
    To me, it's definitely formal.
    
    MARJORIE
    
    It's cozy.
    
    REBECA
    
    You think? Well, I never thought of it like that.
    
    MARJORIE
    
* [A jail.]

    MARJORIE
    
    I thought of a jail.
    
    REBECA
    
    (laugh) A jail?
    
    MARJORIE
    
    (insecure) Well, yes. Because a jail is secure.
    
    REBECA
    
    Yeah, but a jail is supposed to keep people inside. And we're trying to keep them outside.
    
    MARJORIE
    
    I guess you're right.
    
* [I don't know.]

    MARJORIE
    
    I don't know. Not like this at least.
    
    REBECA
    
    Whatever.
    
    MARJORIE
    
    It's like a family just left it after having dinner. It feels... innocent.
    
    REBECA
    
    It's just a house.
    
    MARJORIE
    
-

Umm... I... I wanted to apologize. About the phone?

REBECA

Yeah, whatever.

MARJORIE

It was really a dumb idea, I guess.

REBECA

Hell yeah! You know, it's not only the police that can track those things down! *Especially* organized crime fuckers have *huge* resources!

MARJORIE

I wasn't aware of that.

REBECA

Well now you are. Just stay put, okay?

MARJORIE

Okay.

REBECA

Okay.
    
-> questions

=== start ===

~ temp greeting = RANDOM(1, 3) 

{ greeting == 1:
    MARJORIE
    
    Hey, Rebeca?
    
    REBECA
    
    What?
}
{ greeting == 2:
    MARJORIE
    
    Do you have a second, Rebeca?
    
    REBECA
    
    Yeah, I guess.
}
{ greeting == 3:
    MARJORIE
    
    Can I ask you something, Rebeca?
    
    REBECA
    
    (grunts) If you must.
}

-> questions

=== questions ===

* [About your career]

    -> career
    
* {made_conversation == true} [Can I have my phone back?]

    -> phone
    
* {made_conversation == true and has_seen_padlock == true} [About the padlock.]

    -> padlock
    
* [Thanks.]

    MARJORIE
    
    Thanks, Rebeca.
    
    REBECA
    
    Whatever.
    
    -> END
    
=== career ===

MARJORIE
    
How long have you been with the police?

REBECA

Signed up right after turning 21.

MARJORIE

Sound's like you really wanted this job?

REBECA

Yup. My mom was a cop. I've grown up with the Force, so to say.

MARJORIE

I never knew what I wanted to become. Well, aside from a pet doctor and a princess. (laughs)

REBECA

(...)

MARJORIE

Anyways...

~ made_conversation = true

-> questions

=== phone ===

MARJORIE

Can you give me my phone back?

REBECA

Negative.

MARJORIE

Oh, come on. There are some games on there I could play.

REBECA

Should've thought about that before you put us all in danger here!

MARJORIE

Yes. (beat) Okay. (beat) Sorry.

REBECA

(grunts)

-> questions

=== padlock ===

MARJORIE

I've noticed a padlock on one of the drawers in my room?

REBECA

A padlock?

MARJORIE

Yes, hanging on the drawer.

REBECA

Never noticed it. Did you open it?

MARJORIE

Can't.

REBECA

Weird.

MARJORIE

So you don't know something about it?

REBECA

Nope.

MARJORIE

Hm. Okay.

-> questions