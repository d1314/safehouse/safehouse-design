# Brianna 3

#dialogs #livingroom 

# Comment

In this brianna dialog, Marjorie asks Brianna about Rebeca, because she's her main suspect for trying to scare her to cancel her testimony. Marjorie starts with general questions about Rebeca and then trying more and more to get into possible wrongdoings by Rebeca which is the point at which Brianna asks her if something's wrong and Marjorie backs off and they continue their usual preparations for the testimony.

Things to establish here:

* Rebeca's backstory as a police enthusiast, a very sucessful graduate of the police Academy
* Rebeca is overly correct and has a strong sense for justice
* Rebeca has had some complaints about not acting appropriately in some cases because she can be quite hot-headed (which Marjorie jumps on), but all were minor issues

# Start

> [marjorie](../characters/marjorie.md)
>
> Hello, Brianna.

> [brianna](../characters/brianna.md)
>
> Hello, Marjorie. How are you holding up?

> [marjorie](../characters/marjorie.md)
>
> I'm... holding up I guess. It takes time to get accustomed.

> [brianna](../characters/brianna.md)
>
> I can imagine that.
>
> (beat) Ok then. Should we continue?

> [marjorie](../characters/marjorie.md)
>
> Can I ask you something about Rebeca?

> [brianna](../characters/brianna.md)
>
> (confused) Rebeca? You mean Officer Pliego?

> [marjorie](../characters/marjorie.md)
>
> Yes.

> [brianna](../characters/brianna.md)
>
> (still confused) Okay...? What's on your mind?

[Questions](#questions)

# questions

* [How well do you know her?](#howwell)
* [She doesn't seem to like me.](#doesntlike)

After both questions were answered, the dialog continues:

> [brianna](../characters/brianna.md)
>
> Overall, you can feel safe with Officer Pliego around. She will take good care of you.

> [marjorie](../characters/marjorie.md)
>
> (dark) I guess she will.

> [brianna](../characters/brianna.md)
>
> Marjorie?

> [marjorie](../characters/marjorie.md)
>
> (ignoring her) Allright. Are we going to repeat everything again today?

> [brianna](../characters/brianna.md)
>
> I guess so.

> [marjorie](../characters/marjorie.md)
>
> (sigh) Okay then.

END

# howwell

> [brianna](../characters/brianna.md)
>
> Me? Not well actually. We met for the first time on this case. But I've read her file.

> [marjorie](../characters/marjorie.md)
>
> What does it say?

> [brianna](../characters/brianna.md)
>
> (Reluctantly) That Rebeca is a highly trained and competent officer. She graduated as one of the best of the year in the Academy.

> [marjorie](../characters/marjorie.md)
>
> How old is she?

> [brianna](../characters/brianna.md)
>
> Um... (thinks) 28 I think. But she's very experienced.

[questions](#questions)

# doesntlike

> [brianna](../characters/brianna.md)
>
> Oh, Marjorie. I don't think so.

> [marjorie](../characters/marjorie.md)
>
> She's mean.

> [brianna](../characters/brianna.md)
>
> She's... she's not a people person.

> [marjorie](../characters/marjorie.md)
>
> Yes.

> [brianna](../characters/brianna.md)
>
> I heard she's very... um... correct. Maybe overly correct at times.
>
> But she's very professional. She has a strong sense for justice.

> [marjorie](../characters/marjorie.md)
>
> (grunt) "Correct..."

> [brianna](../characters/brianna.md)
>
> (not noticing Marjorie) Sometimes even a bit too much for our own good.

> [marjorie](../characters/marjorie.md)
>
> What do you mean?

> [brianna](../characters/brianna.md)
>
> Huh?

> [marjorie](../characters/marjorie.md)
>
> "Too much for our own good"? What do you mean?

> [brianna](../characters/brianna.md)
>
> (sigh) She has had some complaints about not acting properly because she can be hot-headed at times.

> [marjorie](../characters/marjorie.md)
>
> (louder) Exactly! Hot-headed. She's a freak!

> [brianna](../characters/brianna.md)
>
> (interrupting) ...but all cases were minor ones and eventually dropped.

> [marjorie](../characters/marjorie.md)
>
> (grunt) Whatever.

[questions](#questions)