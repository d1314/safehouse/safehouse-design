# Day 3 breakfast

#dialogs #livingroom 

# Comment

This dialog is the showdown between Marjorie and Rebeca. On a meta level, it is also the key point for Rebeca where she starts to seriously doubt Calvin motives. She finally storms off. To Marjorie and the player, this looks as if she's angry at them. Actually, Rebeca walks out and investigates more into the case of Jacob Grimes, which lead to the fight between Calvin and her on day 4.

# Start

> [marjorie](../characters/marjorie.md)
>
> How did you two sleep?

> [calvin](../characters/calvin.md)
>
> Honestly, I never sleep good in witness protection. I mean, usually nothing happens, but you're still quite tense.

> [marjorie](../characters/marjorie.md)
>
> I see.

> [calvin](../characters/calvin.md) 
>
> But I manage.

> [marjorie](../characters/marjorie.md)
>
> How did you sleep, Rebeca?

> [rebeca](../characters/rebeca.md)
>
> Huh?

> [marjorie](../characters/marjorie.md)
>
> How did you sleep?

> [rebeca](../characters/rebeca.md)
>
> (shrugs shoulders) okay, I guess.

> [marjorie](../characters/marjorie.md)
>
> Aha.

[Questions](#Questions)

# Questions

* [How can you even keep up with tense situations like this? I'd go crazy.](#keepup)
* [I don't know, if you can tell me that, but: how often are you assigned to witness protection jobs?](#witnessprotection)

After the two questions are asked:

* [Listen, Calvin. I heard about Jacob Grimes. I just wanted to tell you how sorry I am.](#jacob)

# keepup

> [calvin](../characters/calvin.md)
>
> Well, you get some sort of training for things like this, but it's mostly experience and... well, you need to be made for the job, I guess.

> [marjorie](../characters/marjorie.md) 
>
> (laughs) Yeah, I guess, it's no job for me.

> [rebeca](../characters/rebeca.md)
>
> (suddenly, jokingly) It's a job like every other job. It's just not well paid, right, Calvin?

> [calvin](../characters/calvin.md)
>
> (laughs) Oh yes.

> [rebeca](../characters/rebeca.md)
>
> Unless you don't have some other... opportunities, right?

[calvin](../characters/calvin.md) laugh becoms weaker. He looks at her, confused.

> [marjorie](../characters/marjorie.md)
>
> What do you mean?

> [rebeca](../characters/rebeca.md)
>
> Ah, nothing. Nothing.

[rebeca](../characters/rebeca.md) continues to eat.

[Questions](#Questions)

# witnessprotection

> [calvin](../characters/calvin.md)
>
> Phew, I lost count. I was on... maybe thirty.

> [rebeca](../characters/rebeca.md)
>
> It's my third.

> [calvin](../characters/calvin.md)
>
> (laughs) Our fledgling.

> [rebeca](../characters/rebeca.md)
>
> Yeah. Can't beat your experience.

> [calvin](../characters/calvin.md)
>
> You get there.

> [rebeca](../characters/rebeca.md)
>
> With the help of the right people!

> [calvin](../characters/calvin.md)
>
> You have the best chief for that. Lindsey's doing an awesome job.

> [rebeca](../characters/rebeca.md)
>
> Oh, yes. The help of the right people on the force as well. That's true.

[calvin](../characters/calvin.md) is irritated.

> [calvin](../characters/calvin.md)
>
> Ah... yes.

[Questions](#Questions)

# jacob

[calvin](../characters/calvin.md) startles and some seconds pass. [rebeca](../characters/rebeca.md) looks at [calvin](../characters/calvin.md). 

> [calvin](../characters/calvin.md)
>
> I... Thank you, Marjorie.

> [marjorie](../characters/marjorie.md)
>
> I'm sorry. I hope that didn't reopen any old wounds.

> [calvin](../characters/calvin.md)
>
> No. No, it's okay.

> [rebeca](../characters/rebeca.md)
>
> (diabolic) I guess, Jacob couldn't stand the pressure.

> [marjorie](../characters/marjorie.md)
>
> Maybe somebody put more pressure on him.

> [rebeca](../characters/rebeca.md)
>
> (confused) What?

> [marjorie](../characters/marjorie.md)
>
> I mean, if somebody tried to scare *me* over and over again, I don't know if I could stand the pressure either.

> [rebeca](../characters/rebeca.md)
>
> Scare you?

> [marjorie](../characters/marjorie.md)
>
> Yes. I don't know... leaving scary notes lying around for example, Rebeca.

> [rebeca](../characters/rebeca.md)
>
> What... (getting angry) What are you trying to say here?

> [marjorie](../characters/marjorie.md)
>
> (getting angry) Oh, nothing. I just don't like to be scared. I'm scary enough already.

> [rebeca](../characters/rebeca.md)
>
> (getting louder) Well, not scared enough to make an unencrypted phone call to your mother, as it seems.

> [marjorie](../characters/marjorie.md)
>
> (getting louder) Maybe I was just trying to talk to somebody to ease down the pressure. But that approach has been perfectly prevented by you now, as it seems.

> [rebeca](../characters/rebeca.md)
>
> (shouting) You can always talk to us instead.

[rebeca](../characters/rebeca.md) stood up.

> [marjorie](../characters/marjorie.md)
>
> (shouting) As if you would be interested in talking to me!

[marjorie](../characters/marjorie.md) stood up.

END