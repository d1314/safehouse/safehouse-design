# Rebeca 3

#dialogs #kitchen 

# Comment

This dialogs depicts a highly confused Rebeca on the one side and a highly suspicious Marjorie on the other side.

Rebeca is just on the brink of realizing Calvin's role in all this and Marjorie still thinks, Rebeca wants to scare her to give up her testimony.

Marjorie's intention is to get Rebeca's finger prints so she can compare them with the ones from the hook. 

Rebeca's basically just zoned out.

# Start

> [marjorie](../characters/marjorie.md)
>
> Rebeca, I... I need your help.

Pause

> [marjorie](../characters/marjorie.md)
>
> Rebeca?

Pause

> [marjorie](../characters/marjorie.md)
>
> REBECA?

> [rebeca](../characters/rebeca.md)
>
> (snaps out) Huh? What?

> [marjorie](../characters/marjorie.md)
>
> Are you okay? You seem... lost.

> [rebeca](../characters/rebeca.md)
>
> What? No, no. I'm... I'm fine. I just... (breathes) I just have a bad headache.

> [marjorie](../characters/marjorie.md)
>
> Oh, I'm sorry.

> [rebeca](../characters/rebeca.md)
>
> It's okay. I manage.

> [marjorie](../characters/marjorie.md)
>
> Anything you want?

> [rebeca](../characters/rebeca.md)
>
> (jokingly) A Bloody Mary is what I want. Even a Virgin Mary.

> [marjorie](../characters/marjorie.md)
>
> Oh. (laughs)

> [rebeca](../characters/rebeca.md)
>
> But that has to wait after this assignment.

> [marjorie](../characters/marjorie.md)
>
> I see.
> (to herself) A Virgin Mary, eh?

END