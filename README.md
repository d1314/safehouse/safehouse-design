![Safehouse logo](logo.png)

# Safehouse

An atmospheric point and click thriller

## Introduction

Welcome to the design repository of Safehouse. Here you can find the following things:

* [The Safehouse Game Design Document](design/gdd.md)
* [The dialogs from the game](design/dialogs)
* [The blender sources used in the game](design/blender)
* The [preliminary house designs](design/house.sh3d) made with [Sweet Home 3D](http://www.sweethome3d.com/de/)
* The [game's logo design](design/Logo.sketch) made in [Sketch](https://www.sketch.com/)

Sources are released under the [MIT-License](LICENSE), assets are released under the [CC-BY-SA-License](CC-LICENSE.md)

The game itself is available in [its own repository](https://github.com/deep-entertainment/safehouse-game).